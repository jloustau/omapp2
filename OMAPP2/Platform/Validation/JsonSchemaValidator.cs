﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace Vistage.Platform.Validation
{
    public class JsonSchemaValidator
    {
        public static bool Validate<T>(T obj, string schemaFile, out string errorString) where T : class
        {
            var schema = JSchema.Parse(System.IO.File.ReadAllText(schemaFile));
            var ser = JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            var json = JToken.Parse(ser);

            // validate json
            IList<ValidationError> errors;
            bool valid = json.IsValid(schema, out errors);

            var sb = new StringBuilder();
            foreach (var error in errors)
            {
                sb.AppendLine(error.Message);
            }

            errorString = sb.ToString();
            return errors.Count == 0;
        }
    }
}
