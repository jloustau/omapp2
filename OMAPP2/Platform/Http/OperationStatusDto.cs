﻿using System.Net;

namespace Vistage.Platform.Http
{
    public class OperationStatusDto<T> where T : class
    {
        public T Data { get; set; }

        public string Message { get; set; }

        public HttpStatusCode HttpResult { get; set; }

        public bool IsSuccessful { get; set; }
    }
}
