﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Vistage.Platform.Extensions;

namespace Vistage.Platform.Http
{

    public static class Rest
    {
        #region Public Methods

        public static async Task<OperationStatusDto<T>> Get<T>(string baseAddress, string route,
            AuthenticationHeaderValue authHeader = null,
            string urlParams = "") where T : class
        {
            using (var client = new HttpClient())
            {
                var result = new OperationStatusDto<T>();


                if (authHeader != null)
                {
                    client.DefaultRequestHeaders.Authorization = authHeader;
                }

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                client.BaseAddress = new Uri(baseAddress);

                try
                {
                    var requestUri = String.IsNullOrEmpty(urlParams) ? route : route + "?" + urlParams;
                    var response = await client.GetAsync(requestUri);


                    result.HttpResult = response.StatusCode;
                    result.IsSuccessful = response.IsSuccessStatusCode;

                    if (response.IsSuccessStatusCode)
                    {
                        result.Data = await response.Content.ReadAsAsync<T>();
                    }
                    else
                    {
                        dynamic errObj = JObject.Parse(await response.Content.ReadAsStringAsync());
                        result.Message = errObj.Message;
                    }
                }
                catch (Exception ex)
                {
                    result.Message = ex.Message;
                }
                return result;
            }
        }

        public static async Task<OperationStatusDto<T>> Post<T>(string baseAddress, string route, T data,
            AuthenticationHeaderValue authHeader = null) where T : class
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                if (authHeader != null)
                {
                    client.DefaultRequestHeaders.Authorization = authHeader;
                }

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.PostAsJsonAsync(route, data);

                var result = new OperationStatusDto<T>()
                {
                    Data = data,
                    Message = String.Empty,
                    HttpResult = response.StatusCode,
                    IsSuccessful = response.IsSuccessStatusCode
                };

                if (!response.IsSuccessStatusCode)
                {
                    dynamic errObj = JObject.Parse(await response.Content.ReadAsStringAsync());
                    result.Message = errObj.Message;

                    return result;
                }

                result.Data =  await response.Content.ReadAsAsync<T>();
                return result;
            }
        }


        public static async Task<OperationStatusDto<T>> PostWithHeaders<T>(string baseAddress, string route, T data, string token = "",
            Dictionary<string, string> headers = null) where T : class
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                if (!String.IsNullOrEmpty(token))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                }

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpRequestMessage msg = new HttpRequestMessage(HttpMethod.Post, route);
                if (headers != null)
                {
                    foreach (var item in headers.Keys)
                    {
                        msg.Headers.Add(item, headers[item]);
                    }
                }
                msg.Content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8,
                    "application/json");

                var response = await client.SendAsync(msg);
                if (!response.IsSuccessStatusCode)
                {
                    return new OperationStatusDto<T>()
                    {
                        Data = data,
                        Message = String.Empty,
                        HttpResult = response.StatusCode
                    };
                }
                return await response.Content.ReadAsAsync<OperationStatusDto<T>>();
            }
        }

        public static async Task<OperationStatusDto<T>> Put<T>(string baseAddress, string route, T data, string token = "") where T : class
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                if (!String.IsNullOrEmpty(token))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                }
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.PutAsJsonAsync(route, data);

                if (response.IsSuccessStatusCode)
                {
                    return new OperationStatusDto<T>()
                    {
                        Data = data,
                        Message = String.Empty,
                        HttpResult = response.StatusCode
                    };
                }

                return new OperationStatusDto<T>()
                {
                    Data = data,
                    Message = String.Empty,
                    HttpResult = response.StatusCode
                };
            }


        }

       
        public static async Task<OperationStatusDto<T2>> Patch<T1, T2>(string baseAddress, string route, T1 data,
            AuthenticationHeaderValue authHeader = null) 
            where T1 : class
            where T2 : class 
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseAddress);
                if (authHeader != null)
                {
                    client.DefaultRequestHeaders.Authorization = authHeader;
                }

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.PatchAsJsonAsync(route, data);

                var result = new OperationStatusDto<T2>()
                {
                    Message = String.Empty,
                    HttpResult = response.StatusCode,
                    IsSuccessful = response.IsSuccessStatusCode
                };

                if (!response.IsSuccessStatusCode)
                {
                    dynamic errObj =  JObject.Parse(await response.Content.ReadAsStringAsync());
                    result.Message = errObj.message;

                    return result;
                }

                result.Data = await response.Content.ReadAsAsync<T2>();
                return result;
            }
        }

        public static AuthenticationHeaderValue CreateBasicAuthentication(string user, string password)
        {
            var auth = new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(Encoding.Default.GetBytes(user + ":" + password)));

            return auth;
        }

        #endregion
    }
}
