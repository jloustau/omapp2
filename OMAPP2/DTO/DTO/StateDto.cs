﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Vistage.Membership.Dto
{
    public class StateDto
    {
        //public string countryCode { get; set; }
        //    public string chRegionCode { get; set; }
        //   public int iSiteId { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "stateCode")]
        public string StateCode { get; set; }
    
  //      public Nullable<int> iAlternateLangId { get; set; }
  //      public Nullable<System.DateTime> dtModifiedDate { get; set; }
  //      public byte tiRecordStatus { get; set; }
    }


    public class StatesDto
    {

        [JsonProperty(PropertyName = "states")]
        public StateDto[] states { get; set; }
    }
}