﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace Vistage.Membership.Dto
{

    public class PaymentResult
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int ccAuthorizationResponsesCode { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool ccProcessingSuccess { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string resultText { get; set; }
    }

    public class UpdateMembershipRequestDto
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool stepOneSaved { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool termsAccepted { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string termsVersion { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string termsFilePath { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool stepTwoSaved { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public PersonUpdate person { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public CompanyUpdate company { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool stepThreeSaved { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string methodOfPayment { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string billingFrequency { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public BillingInfoUpdate billingInfo { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool financialTermsAccepted { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string financialTermsAcceptanceName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool submitted { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool ccProcessingSuccess { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int ccAuthorizationResponsesCode { get; set; }
    }

    public class PersonUpdate
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string sfstringType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string sfstringId { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string prefix { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string first { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string middleInitial { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string last { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string suffix { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string preferredName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string jobTitle { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string workPhone { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string mobilePhone { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string email { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string birthday { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string gender { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string spouse { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string referralType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string referralName { get; set; }
    }

    public class BillingInfoUpdate
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string companyName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string address1 { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string address2 { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string city { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string state { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string zip { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string country { get; set; }
    }

    public class CompanyUpdate
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string businessDescription { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string typeOfOrg { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string annualRevenue { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string numberOfEmployee { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string address1 { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string address2 { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string city { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string state { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string zip { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string country { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string website { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string industryNAICS { get; set; }
    }

}
