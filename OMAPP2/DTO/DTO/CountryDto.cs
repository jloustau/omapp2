﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace Vistage.Membership.Dto
{
    public class CountryDto
    {
 
  //      public int CountryId { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "countryCode")]
        public string CountryCode { get; set; }

      //  public Nullable<int> TimeZoneID { get; set; }
    }

    public class CountriesDto
    {
        [JsonProperty(PropertyName = "countries")]
        public CountryDto[] Countries { get; set; }
    }
}
