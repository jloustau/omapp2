﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vistage.Membership.Dto
{
    public class IndustriesDto
    {
        public Industry[] industries { get; set; }
    }

    public class Industry
    {
        public string description { get; set; }
        public int code { get; set; }
        public IndustryDetail[] industryDetail { get; set; }

    }

    public class IndustryDetail
    {
        public string description { get; set; }
        public int code { get; set; }

        public int? parent { get; set; }
    }



    public class IndustryDetailDto
    {
        public IndustryDetail industryDetail { get; set; }
    }

}
