﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Vistage.Membership.Dto
{
    public class MembershipApplication
    {
        public int membershipApplicationId { get; set; }
        public bool stepOneSaved { get; set; }
        public bool termsAccepted { get; set; }
        public string termsAcceptedTimestamp { get; set; }
        public string termsVersion { get; set; }
        public string termsFilePath { get; set; }
        public bool stepTwoSaved { get; set; }
        public Person person { get; set; }
        public Company company { get; set; }
        public bool stepThreeSaved { get; set; }
        public bool isSponsored { get; set; }
        public int sponsorVistageId { get; set; }
        public string vistageProgram { get; set; }
        public string[] availablePaymentMethod { get; set; }
        public string methodOfPayment { get; set; }
        public string billingRateVersion { get; set; }
        public string billingFrequency { get; set; }
        public Billingrateoption[] billingRateOption { get; set; }
        public Billinginfo billingInfo { get; set; }
        public string enrollmentFee { get; set; }
        public Applieddiscount[] appliedDiscounts { get; set; }
        public bool financialTermsAccepted { get; set; }
        public string financialTermsAcceptanceName { get; set; }
        public string financialTermsTimestamp { get; set; }
        public bool submitted { get; set; }
        public string submittedTimestamp { get; set; }
        public bool ccProcessingSuccess { get; set; }
        public int ccAuthorizationResponsesCode { get; set; }
        public string customerID { get; set; }
        public string submittedIP { get; set; }
        public string startDate { get; set; }

    }

    public class Person
    {
        public string sfstringType { get; set; }
        public string sfstringId { get; set; }
        public string prefix { get; set; }
        public string first { get; set; }
        public string middleInitial { get; set; }
        public string last { get; set; }
        public string suffix { get; set; }
        public string preferredName { get; set; }
        public string jobTitle { get; set; }
        public string workPhone { get; set; }
        public string mobilePhone { get; set; }
        public string email { get; set; }
        public string birthday { get; set; }
        public string gender { get; set; }
        public string spouse { get; set; }
        public string referralType { get; set; }
        public string referralName { get; set; }
    }

    public class Company
    {
        public string name { get; set; }
        public string businessDescription { get; set; }
        public string typeOfOrg { get; set; }
        public string annualRevenue { get; set; }
        public string numberOfEmployee { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
        public string website { get; set; }
        public string industryNAICS { get; set; }
    }

    public class Billinginfo
    {
        public string companyName { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
    }

    public class Billingrateoption
    {
        public string frequency { get; set; }
        public string label { get; set; }
        public string amount { get; set; }
        public string[] methodOfPayment { get; set; }
    }

    public class Applieddiscount
    {
        public int id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public float? amount { get; set; }
        public int? percent { get; set; }
    }

}
