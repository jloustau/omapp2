﻿using System;
using System.Net.Http.Headers;
using System.Text;
using Vistage.Platform.Http;

namespace Vistage.Services.Common
{
    public abstract class MaestroClientService
    {
        protected readonly string User;
        protected readonly string Password;
        protected readonly string MaestroUrl;

        protected MaestroClientService(string maestroUrl, string user, string password)
        {
            MaestroUrl = maestroUrl;
            User = user;
            Password = password;
        }


        /// <summary>
        /// Creates the basic authentication.
        /// </summary>
        /// <returns></returns>
        protected AuthenticationHeaderValue CreateBasicAuthentication()
        {
            var auth = new AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(Encoding.Default.GetBytes(User + ":" + Password)));

            return auth;
        }
    }
}
