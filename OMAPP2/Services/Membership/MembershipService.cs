﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Vistage.Membership.Dto;
using Vistage.Platform.Http;
using Vistage.Services.Payments;
using Vistage.Services.Common;

namespace Vistage.Services.Membership
{
    public class MembershipService : MaestroClientService, IMembershipService
    {
        #region Fields

        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes a new instance of the <see cref="MembershipService"/> class.
        /// </summary>
        /// <param name="url">The maestro geo URL.</param>
        /// <param name="user">The user.</param>
        /// <param name="password">The password.</param>
        /// <param name="paymentProcessor">The payment processor.</param>
        public MembershipService(string url, string user, string password, IPaymentProcessor paymentProcessor) : base(url, user, password)
        {
        }
      
        /// <summary>
        /// Validates the application.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        public async Task<OperationStatusDto<MembershipApplication>> ValidateApplication(string token)
        {
            var result = await Rest.Get<MembershipApplication>(MaestroUrl, @"validate/" + token, CreateBasicAuthentication());
            return result;
        }

        /// <summary>
        /// Gets the application.
        /// </summary>
        /// <param name="membershipId">The membership identifier.</param>
        /// <returns></returns>
        public async Task<OperationStatusDto<MembershipApplication>> GetApplication(string membershipId)
        {
            var result = await Rest.Get<MembershipApplication>(MaestroUrl, membershipId, CreateBasicAuthentication());
            return result;
        }

        /// <summary>
        /// Saves the application.
        /// </summary>
        /// <param name="membershipApplicationId">The membership application identifier.</param>
        /// <param name="request">The membership application.</param>
        /// <returns></returns>
        public async Task<OperationStatusDto<MembershipApplication>> SaveApplication(string membershipApplicationId, UpdateMembershipRequestDto request)
        {
            
            var result = await Rest.Patch<UpdateMembershipRequestDto, MembershipApplication> (MaestroUrl, membershipApplicationId, request, CreateBasicAuthentication());

            return result;
        }

        #endregion

        #region Private Methods 


        private BillingData ConvertBillingInfo(MembershipApplication membership )
        {
            var billingData = new BillingData();
            var billingRateOption = membership.billingRateOption.SingleOrDefault(e => e.frequency == membership.billingFrequency);

            if (billingRateOption != null)
            {
                billingData.Amount = billingRateOption.amount;
                billingData.PaymentMethod = membership.methodOfPayment;
                billingData.BillingAddress1 = membership.billingInfo.address1;
                billingData.BillingAddress2 = membership.billingInfo.address2;
                billingData.BillingCity = membership.billingInfo.city;
                billingData.BillingState = membership.billingInfo.state;
                billingData.BillingCountryCode = membership.billingInfo.country;
                billingData.BillingZip = membership.billingInfo.zip;

                switch (membership.methodOfPayment)
                {
                    case "CC":
                        billingData.PaymentMethod = PaymentMethod.CreditCard;
                        break;

                    case "EFT":
                        billingData.PaymentMethod = PaymentMethod.Eft;
                        break;

                    case "INV":
                        billingData.PaymentMethod = PaymentMethod.Invoicing;
                        break;

                    default:
                        throw new ArgumentException("Payment Method not valid. " + membership.methodOfPayment);
                }

                billingData.AccountName = membership.billingInfo.companyName;
            }



            return billingData;
        }

        #endregion
    }
}
