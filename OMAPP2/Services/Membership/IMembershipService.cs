﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vistage.Membership.Dto;
using Vistage.Platform.Http;
using Vistage.Services.Payments;

namespace Vistage.Services.Membership
{
    public interface IMembershipService
    {
        Task<OperationStatusDto<MembershipApplication>> ValidateApplication(string token);

        Task<OperationStatusDto<MembershipApplication>> GetApplication(string membershipId);

        Task<OperationStatusDto<MembershipApplication>> SaveApplication(string membershipApplicationId,
            UpdateMembershipRequestDto request);

    }
}
