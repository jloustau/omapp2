﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vistage.Membership.Dto;
using Vistage.Platform.Http;

namespace Vistage.Services.Geo
{
    interface IGeoService
    {
        Task<OperationStatusDto<CountriesDto>> GetCountries();
        Task<OperationStatusDto<StatesDto>> GetStates(string countryCode);
    }
}
