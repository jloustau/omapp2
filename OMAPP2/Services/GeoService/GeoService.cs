﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Vistage.Membership.Dto;
using Vistage.Platform.Http;
using Vistage.Services.Common;

namespace Vistage.Services.Geo
{
    public class GeoService : MaestroClientService, IGeoService
    {
        private readonly string _maestroGeoUrl;


        public GeoService(string maestroGeoUrl, string user, string password) : base(maestroGeoUrl, user, password)
        {
            _maestroGeoUrl = maestroGeoUrl;
        }

        public async Task<OperationStatusDto<CountriesDto>> GetCountries()
        {
            var result = await Rest.Get<CountriesDto>(_maestroGeoUrl, "country", CreateBasicAuthentication());
            return result;
        }

        public async Task<OperationStatusDto<StatesDto>> GetStates(string countryCode)
        {
            var result = await Rest.Get<StatesDto>(_maestroGeoUrl, "country/" + countryCode + "/state", CreateBasicAuthentication());
            return result;
        }


      
    }
}
