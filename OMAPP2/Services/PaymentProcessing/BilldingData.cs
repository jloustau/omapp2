﻿namespace Vistage.Services.Payments
{


    public enum PaymentProcessingStatus
    {
        Approved = 1000,
        Failed =2000
    }


    public enum TransactionStatus
    {
        Success = 0,
        Failed
    }

    public enum PaymentType
    {
        CreditCard = 0,
        Invoice,
        Eft
    };

    public static class PaymentMethod
    {
        public const string CreditCard = "Credit Card";
        public const string Eft = "Electronic fund transfer";
        public const string Invoicing = "Invoicing";
    }

    public static class CcType
    {
        public const string Visa = "VISA";
        public const string MasterCard = "MC";
        public const string Amex = "AMEX";
    }

    public class BillingData
    {
        public string Amount { get; set; }
        public string CreditCardNumber { get; set; }
        public string AccountName { get; set; }
        public string ContactName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string BillingAddress1 { get; set; }
        public string BillingAddress2 { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingZip { get; set; }
        public string BillingCountryCode { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public string WorkPhone { get; set; }
        public string PhoneExtension { get; set; }
        public string CountryCode { get; set; }
        public string FailOnAddressMismatch { get; set; }
        public string FailOnZipMismatch { get; set; }
        public string CustomerCode { get; set; }
        public string MSO_Default { get; set; }
        public string SaveCreditCard { get; set; }
        public string CCExpirationData { get; set; }
        public string CCType { get; set; } // for Dev "Retail Credit" for prod "Visa" or etc...
        public string PaymentMethod { get; set; }
        public string FinancialInstitution { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
    }
}
