﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vistage.Services.Payments
{
    public interface IPaymentProcessor
    {
        bool ValidateSchema(BillingData billingData, out string err );

        TransactionStatus ProcessPayment( BillingData billingData, out int result, ref string errorString, string mode, string environment);

        TransactionStatus CashReceiptPost( BillingData billingData, out int result, ref string errorString, string mode, string environment);
    }
}
