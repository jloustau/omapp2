﻿using System;
using System.ComponentModel;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Vistage.Platform.Validation;

namespace Vistage.Services.Payments.Nodus
{
    public class NodusPaymentProcessor : IPaymentProcessor
    {
        #region Fields
        private static readonly string testAmount = "999.99";
        private static string _setupId;
        #endregion

        #region Constrcutors
        public NodusPaymentProcessor()
        {
            var config = new ConfigurationBuilder().AddJsonFile("Nodus.json");
            var configuration = config.Build();
            _setupId = configuration["setupId"];
        }

        #endregion

        #region Public Methods
        public TransactionStatus ProcessPayment(BillingData billingData, out int result, ref string errorString, string mode, string environment)
        {
            var config = new ConfigurationBuilder().AddJsonFile("Nodus.json");
            var configuration = config.Build();
            _setupId = configuration["setupId"];

            if (billingData.PaymentMethod == PaymentMethod.CreditCard && environment != "dev")
            {
                return CashReceiptPost(billingData, out result, ref errorString, mode, environment);
            }
            result = 0;
            return CustomerPost(billingData, ref errorString, mode);
        }

        public TransactionStatus CashReceiptPost(BillingData billingData, out int result, ref string errorString, string mode, string environment)
        {
            TransactServer.Transaction trx = OnlinePayment(billingData, out result, mode, environment);

            //var foo2 = trx.FailureFields.XML;
            //foreach (var item in trx.StatusFields)
            //{


            //}
            //var foo3 = trx.TransactionFields.XML;
            //var msg = trx.TransactionFields.ItemByName["ResponseMsg"];
            //foreach (var item in trx.TransactionFields)
            //{
            //    string foo = item.ToString();

            //}
            //var foo4 = trx.ResponseFields.XML;
            //foreach (var item in trx.FailureFields)
            //{
            //    string foo = item.ToString();

            //}
            //var foo5 = trx.StatusFields.XML;

            errorString = trx.TransactionFields.ItemByName["ResponseMsg"].Value;

            //Proceed only if transaction was approved
            if (result != trx.Status_Approved)
            {
                return TransactionStatus.Failed;
            }

            //Create Customer
            CustomerPost(billingData, ref errorString, mode);


            //Protect this section against unexpected errors
            //In the event of an error issue a void transaction
            try
            {
                //Initialize eStore objects
                ConnectionServer.Connections objConnections = new ConnectionServer.Connections();
                ConnectionServer.Connection objConnection = objConnections[_setupId];
                eStoreAdapter.TransactionAdapter eStoreTAdapter = new eStoreAdapter.TransactionAdapterClass();
                string connectionString = objConnection.Adapters[eStoreTAdapter.AdapterName].ConnectionString;

                //Make a connection.
                if (!eStoreTAdapter.Connect(ref connectionString, ref _setupId))
                {
                    Globals.Logger.InfoFormat("CashReceiptPost(), eStoreAdapter connection error: {0}", eStoreTAdapter.ErrorString);

                    int trxVoidResponse = int.MinValue;

                    //Attempt to void the original transaction
                    VoidOnlinePayment(_setupId, trx.ResponseFields.get_ItemByName("OriginationID").Value, billingData, out trxVoidResponse, mode, environment);

                    if (trxVoidResponse == trx.Status_Approved)
                    {
                        if (mode == "test")
                        {
                            Globals.Logger.InfoFormat("CashReceiptPost(). Voiding original transaction: {0}", trxVoidResponse);
                        }
                    }
                    else
                    {
                        Globals.Logger.InfoFormat("CashReceiptPost(), Voiding original transaction failed: {0}", trxVoidResponse);
                    }

                    return TransactionStatus.Failed;
                }

                //Generate the CashReceipt
                eStoreAdapter.CashReceipt cashReceipt = eStoreTAdapter.CashReceipt;

                String sTotal = billingData.Amount;
                sTotal = (mode != null && mode == "test") ? testAmount : sTotal;
                DateTime dtTest = DateTime.Today;

                String sMonthInDigit = dtTest.Month.ToString();
                if (Convert.ToInt16(sMonthInDigit) < 10)
                {
                    sMonthInDigit = "0" + sMonthInDigit;
                }

                String sDayInDigit = dtTest.Day.ToString();
                if (Convert.ToInt16(sDayInDigit) < 10)
                {
                    sDayInDigit = "0" + sDayInDigit;
                }

                var sTodayDate = DateTime.Today.Year.ToString() + sMonthInDigit + sDayInDigit;

                cashReceipt.AddNew();
                cashReceipt.set_Field("CustomerID", billingData.CustomerCode);
                cashReceipt.set_Field("DocumentID", "*");
                cashReceipt.set_Field("Date", DateTime.Today.Date.ToString());
                cashReceipt.set_Field("Checkbook", "PETTY CASH");
                cashReceipt.set_Field("TrxAmount", sTotal);
                cashReceipt.set_Field("BatchID", "MEMAPP" + sTodayDate);
                cashReceipt.set_Field("Type", cashReceipt.CashReceiptType_Credit.ToString());
                cashReceipt.set_Field("Description", "Membership Signup");
                cashReceipt.set_Field("UserID", "NodusPaymentProcessor");
                cashReceipt.set_Field("CurrencyID", "Z-US$");
                cashReceipt.set_Field("Token", trx.ResponseFields.get_ItemByName("OriginationID").Value.ToString());
                cashReceipt.set_Field("CreateDist", "0");
                cashReceipt.set_Field("RmdTypal", "9");
                cashReceipt.set_Field("CashDistType", "1");
                cashReceipt.set_Field("CashAcct", "00-000-102500-00"); //Vistage Account
                cashReceipt.set_Field("CashDebit", sTotal);
                cashReceipt.set_Field("CashCredit", "0");
                cashReceipt.set_Field("RecvDistType", "3");
                cashReceipt.set_Field("RecvAcct", "00-000-120000-00"); //Vistage Account
                cashReceipt.set_Field("RecvDebit", "0");
                cashReceipt.set_Field("RecvCredit", sTotal);


                //Add a PaymentLine to this CashReceipt
                object objAddToCashReceipt = trx;

                cashReceipt.AddPaymentLine(ref objAddToCashReceipt);

                //Finally, send the whole document to GP.
                if (cashReceipt.Post())
                {
                    if (mode == "test")
                    {
                        Globals.Logger.Info("CashReceiptPost(), CashReceipt posted to GP successfully");
                    }
                }
                else
                {
                    var sResult = trx.ErrorString;
                    int trxVoidResponse = int.MinValue;

                    errorString = "Sorry! We could not process your payment. Please re-enter your payment information." + sResult;

                    //Attempt to void the original transaction
                    VoidOnlinePayment(_setupId, trx.ResponseFields.get_ItemByName("OriginationID").Value, billingData, out trxVoidResponse, mode, environment);

                    //Console.WriteLine("CashReceipt failed to post to GP.\n");
                    Globals.Logger.Info("CashReceiptPost(), CashReceipt failed to post to GP");

                    if (trxVoidResponse == trx.Status_Approved)
                    {
                        if (mode == "test")
                        {
                            Globals.Logger.InfoFormat("CashReceiptPost(), Original Sale transaction was reversed after Cash Reciept failed to post to GP: {0}", trxVoidResponse);
                        }
                    }
                    else
                    {
                        Globals.Logger.InfoFormat(
                            "CashReceiptPost(), Original Sale transaction was NOT reversed after Cash Reciept failed to post to GP: {0}",
                            eStoreTAdapter.ErrorString);
                    }
                    return TransactionStatus.Failed;
                }

                return TransactionStatus.Success;

            }
            catch (Exception ex)
            {
                Globals.Logger.Error(ex);
                int trxVoidResponse = int.MinValue;
                String sError = trx.ErrorString;
                //Attempt to void the original transaction
                VoidOnlinePayment(_setupId, trx.ResponseFields.get_ItemByName("OriginationID").Value, billingData, out trxVoidResponse, mode, environment);
            }

            return TransactionStatus.Failed;

        }


        private TransactServer.Transaction VoidOnlinePayment(string setupId, string originationId, BillingData billingData, out int result,
            string mode, string environment)
        {
            //Initialize eStore objects
            TransactServer.Connection tconn = new TransactServer.ConnectionClass();
            tconn.Connect(setupId);
            TransactServer.Transaction trx = tconn.OpenTransaction(tconn.TransactionType_Void, false);

            String sTotal = billingData.Amount;
            sTotal = (mode != null && mode == "test") ? testAmount : sTotal;
            String sCard = billingData.CreditCardNumber.Replace(" ", "");

            String sAccountName = "";
            String sAccountStreet1 = "";
            String sAccountStreet2 = "";
            String sCity = "";
            String sState = "";
            String sZip = "";

            sAccountName = billingData.AccountName;
            sAccountStreet1 = billingData.Address1;
            sAccountStreet2 = billingData.Address2;
            sCity = billingData.BillingCity;
            sState = billingData.BillingState;
            sZip = billingData.BillingZip;

            //Set Common Request Properties
            trx.SetField("TrxAmount", sTotal);
            trx.SetField("AccountName", sAccountName);
            trx.SetField("FirstName", billingData.FirstName);
            trx.SetField("LastName", billingData.LastName);
            trx.SetField("Address1", sAccountStreet1);
            trx.SetField("Address2", sAccountStreet2);
            trx.SetField("City", sCity);
            trx.SetField("State", sState);
            trx.SetField("Zip", sZip);
            trx.SetField("Email", billingData.Email);
            trx.SetField("FailOnAddressMismatch", "0");
            trx.SetField("FailOnZipMismatch", "0");
            trx.SetField("CustomerCode", billingData.CustomerCode);
            trx.SetField("SaveCreditCard", "1");

            //Set Credit Card Request Properties
            if (environment != null && environment == "dev")
            {
                trx.SetField("CCType", "Retail Credit"); //Required
            }
            else
            {
                trx.SetField("CCType", billingData.CCType); //Required
            }

            trx.SetField("CCType", billingData.CCType); //Required
            trx.SetField("CCNumber", sCard); //Required
            //trx.SetField("CVV2", "123"); //not stored
            trx.SetField("CCExpDate", billingData.CCExpirationData); //Required

            //Set OriginationID to do a "referenced void/credit"
            trx.SetField("OriginationID", originationId);


            string testXML = trx.XML;
            //Send transaction to Payment Gateway for processing
            result = trx.Process();

            return trx;
        }

        private TransactionStatus CustomerPost(BillingData billingData, ref string errString, string mode)
        {
            //Initialize eStore objects
            var objConnections = new ConnectionServer.Connections();
            var objConnection = objConnections[_setupId];
            eStoreAdapter.TransactionAdapter eStoreTAdapter = new eStoreAdapter.TransactionAdapterClass();
            var connectionString = objConnection.Adapters[eStoreTAdapter.AdapterName].ConnectionString;

            //Connect
            if (!eStoreTAdapter.Connect(ref connectionString, ref _setupId))
            {
                errString +=  ". Error creating customer. Please contact administrator.";
                Globals.Logger.InfoFormat("CustomerPost(), CashReceiptPost(), eStoreAdapter connection error: {0}", eStoreTAdapter.ErrorString);
                return TransactionStatus.Failed;
            }

            string sPhone1 = billingData.WorkPhone;
            string sPhone2 = billingData.MobilePhone;
            string sExt = billingData.PhoneExtension;

            if (String.IsNullOrEmpty(sExt))
            {
                sExt = "0000";
            }

            sPhone1 = sPhone1.Replace("(", "");
            sPhone1 = sPhone1.Replace(")", "");
            sPhone1 = sPhone1.Replace("-", "");
            sPhone1 = sPhone1.Replace(" ", "");
            sPhone1 = sPhone1 + sExt;

            sPhone2 = sPhone2.Replace("(", "");
            sPhone2 = sPhone2.Replace(")", "");
            sPhone2 = sPhone2.Replace("-", "");
            sPhone2 = sPhone2.Replace(" ", "");
            sPhone2 = sPhone2 + "0000";


            //Generate the Customer
            eStoreAdapter.Customer customer = eStoreTAdapter.Customer;

            customer.AddNew();
            customer.set_Field("CustomerID", billingData.CustomerCode);
            customer.set_Field("Name", billingData.FirstName + " " + billingData.LastName);
            customer.set_Field("ShortName", billingData.FirstName + " " + billingData.LastName);
            customer.set_Field("ClassID", "99999");
            customer.set_Field("TaxScheduleID", "");
            customer.set_Field("ShippingMethod", "");
            customer.set_Field("SalesTerritoryID", "");
            customer.set_Field("SalesPersonID", "");
            customer.set_Field("CheckBookID", "");
            customer.set_Field("PaymentTerms", "");
            customer.set_Field("StatementName", "");
            customer.set_Field("Comment1", "");
            customer.set_Field("Comment2", "");
            customer.set_Field("CreditType", "");
            customer.set_Field("CreditLimit", "");
            customer.set_Field("CurrencyID", "");
            customer.set_Field("UpdateIfExists", "1");
            customer.set_Field("CreateAddress", "1");
            customer.set_Field("SendEmailStatements", "1");
            customer.set_Field("UserDefined1", "P");
            customer.set_Field("UserDefined2", "99999");
            customer.set_Field("ToEmailRecipient", billingData.Email);

            //Generate an Address
            customer.set_Field("Address/AddressID", "PRIMARY");
            customer.set_Field("Address/Contact", "Attn: Accounts Payable");
            customer.set_Field("Address/Address1", billingData.Address1);
            customer.set_Field("Address/Address2", billingData.Address2);
            customer.set_Field("Address/Address3", "");
            customer.set_Field("Address/City", billingData.City);
            customer.set_Field("Address/State", billingData.State);
            customer.set_Field("Address/ZipCode", billingData.Zip);
            customer.set_Field("Address/CCode", "US");
            customer.set_Field("Address/Country", billingData.CountryCode);
            customer.set_Field("Address/PhoneNumber1", sPhone1);
            customer.set_Field("Address/PhoneNumber2", sPhone2);
            customer.set_Field("Address/PhoneNumber3", "");
            customer.set_Field("Address/FaxNumber", "");
            customer.set_Field("Address/UserDefined1", "P");
            customer.set_Field("Address/UserDefined2", "99999");


            //Finally, send the document to GP.
            if (customer.Post())
            {
                //Console.WriteLine("Customer posted to GP successfully.\n" + eStoreTAdapter.TransformedXML);
                //sResult = eStoreTAdapter.TransformedXML.ToString();
                if (mode == "test")
                {
                    Globals.Logger.Info("CustomerPost(), Customer posted to GP successfully.");
                }
                return TransactionStatus.Success;
            }
            errString += $". Customer failed to post to GP: {eStoreTAdapter.ErrorString}";
            Globals.Logger.InfoFormat(errString);
            return TransactionStatus.Failed;
        }
        #endregion

        #region Private Methods
        private TransactServer.Transaction OnlinePayment(BillingData billingData, out int result, string mode, string environment)
        {
            //Initialize eStore objects
            TransactServer.Connection tconn = new TransactServer.Connection();
            tconn.Connect(_setupId);
            TransactServer.Transaction trx = tconn.OpenTransaction(tconn.TransactionType_Sale, false);

            String sTotal = billingData.Amount;
            sTotal = (mode != null && mode == "test") ? testAmount : sTotal;
            String sCard = billingData.CreditCardNumber.Replace(" ", "");

            String sAccountName = "";
            String sAccountStreet1 = "";
            String sAccountStreet2 = "";
            String sCity = "";
            String sState = "";
            String sZip = "";

            sAccountName = billingData.AccountName;
            sAccountStreet1 = billingData.BillingAddress1;
            sAccountStreet2 = billingData.BillingAddress2;
            sCity = billingData.BillingCity;
            sState = billingData.BillingState;
            sZip = billingData.BillingZip;

            String sPhone3 = billingData.WorkPhone;
            String sExt3 = billingData.PhoneExtension;
            if (String.IsNullOrEmpty(sExt3))
            {
                sExt3 = "0000";
            }

            sPhone3 = sPhone3.Replace("(", "");
            sPhone3 = sPhone3.Replace(")", "");
            sPhone3 = sPhone3.Replace("-", "");
            sPhone3 = sPhone3.Replace(" ", "");
            sPhone3 = sPhone3 + sExt3;

            //Set Common Request Properties
            trx.SetField("TrxAmount", sTotal);
            trx.SetField("AccountName", sAccountName);
            trx.SetField("FirstName", billingData.FirstName);
            trx.SetField("LastName", billingData.LastName);
            trx.SetField("Address1", sAccountStreet1);
            trx.SetField("Address2", sAccountStreet2);
            trx.SetField("City", sCity);
            trx.SetField("State", sState);
            trx.SetField("Zip", sZip);
            trx.SetField("Email", billingData.Email);
            trx.SetField("Phone", sPhone3);
            trx.SetField("CountryCode", billingData.BillingCountryCode);
            trx.SetField("FailOnAddressMismatch", "0");
            trx.SetField("FailOnZipMismatch", "0");
            trx.SetField("CustomerCode", billingData.CustomerCode);
            trx.SetField("MSO_Default", "1");
            trx.SetField("SaveCreditCard", "1");

            //Set Credit Card Request Properties
            if (environment != null && environment == "dev")
            {
                trx.SetField("CCType", "Retail Credit"); //Required
            }
            else
            {
                trx.SetField("CCType", billingData.CCType); //Required
            }





            trx.SetField("CCNumber", sCard); //Required
            //trx.SetField("CVV2", "123"); //not stored
            trx.SetField("CCExpDate", billingData.CCExpirationData); //Required

            //Send transaction to Payment Gateway for processing

            string testXML = trx.XML;

            result = trx.Process();
            if (result == trx.Status_Approved)
            {
                trx.Post();
                //String sResult2 = "";
                //sResult2 = trx.ErrorString;
            }
            return trx;
        }

        public bool ValidateSchema(BillingData billingData, out string err)
        {
            if (billingData.PaymentMethod == PaymentMethod.CreditCard)
            {
                return JsonSchemaValidator.Validate(billingData, System.Web.Hosting.HostingEnvironment.MapPath("~/Schemas/CC-Schema.json"),
                    out err);
            }

            if (billingData.PaymentMethod == PaymentMethod.Invoicing)
            {
                return JsonSchemaValidator.Validate(billingData, System.Web.Hosting.HostingEnvironment.MapPath("~/Schemas/Invoicing-Schema.json"),
                    out err);
            }

            if (billingData.PaymentMethod == PaymentMethod.Eft)
            {
                return JsonSchemaValidator.Validate(billingData, System.Web.Hosting.HostingEnvironment.MapPath("~/Schemas/EFT-Schema.json"),
                    out err);
            }
            err = "Invalid Payment Methods:" + billingData.PaymentMethod;
            return false;
        }

        #endregion
    }
}
