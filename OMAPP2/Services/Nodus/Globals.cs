﻿using log4net;

namespace Vistage.Services.Payments.Nodus
{
    internal class Globals
    {
        internal static ILog Logger = LogManager.GetLogger("NodusPaymentProcessor");
    }
}
