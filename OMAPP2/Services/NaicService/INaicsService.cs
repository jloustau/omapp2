﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vistage.Membership.Dto;
using Vistage.Platform.Http;

namespace Vistage.Services.Naics
{
    interface INaicsService
    {
        Task<OperationStatusDto<IndustriesDto>> GetIndustries();
        Task<OperationStatusDto<IndustryDetailDto>> GetIndustry(string id);
    }
}
