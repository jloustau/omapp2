﻿using System.Threading.Tasks;
using Vistage.Membership.Dto;
using Vistage.Platform.Http;
using Vistage.Services.Common;

namespace Vistage.Services.Naics
{
    public class NaicsService : MaestroClientService, INaicsService
    {
        public NaicsService(string url, string user, string password) : base(url, user, password)
        {
        }

        public async Task<OperationStatusDto<IndustriesDto>> GetIndustries()
        {
            var result = await Rest.Get<IndustriesDto>(MaestroUrl, "industry", CreateBasicAuthentication());
            return result;
        }


        public async Task<OperationStatusDto<IndustryDetailDto>> GetIndustry(string id)
        {
            var param =  "industry/" + id;
            var result = await Rest.Get<IndustryDetailDto>(MaestroUrl, param, CreateBasicAuthentication());
            return result;
        }
    }
}
