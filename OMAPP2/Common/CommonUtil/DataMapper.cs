﻿using MembershipModel;
using System.Collections.Generic;
using Vistage.Membership.Dto;

namespace Vistage.Membership.Common
{
    public static class DataMapper
    {
        public static CountryDto ToDo(Country entity)
        {
            var dto = new CountryDto
            {
                CountryCode = entity.CountryCode,
            //    CountryId = entity.CountryId,
                Name = entity.Name,
           //     TimeZoneID = entity.TimeZoneID
            };

            return dto;
        }

        public static List<CountryDto> ToDtos(List<Country> entities)
        {
            var dtos = new List<CountryDto>();
            entities.ForEach(e => dtos.Add(ToDo(e)));

            return dtos;
        }

        //public static List<T> ToDtos<T,K>(List<K> entities) 
        //    where T : class
        //    where K:class
        //{
        //    var list = new List<T>();
        //    entities.ForEach(e => list.Add(ToDo(e)));

        //    return list;
        //}

        public static List<NaicDto> ToDtos(List<Naic> entities)
        {
            var dtos = new List<NaicDto>();
            entities.ForEach(e => dtos.Add(ToDo(e)));

            return dtos;
        }

        public static NaicDto ToDo(Naic entity)
        {
            var dto = new NaicDto
            {
                Code = entity.Code,
                Description = entity.Description,
                DisplayOrder = entity.DisplayOrder,
                Parent = entity.Parent
            };

            return dto;
        }

    }
}
