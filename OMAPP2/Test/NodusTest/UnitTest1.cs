﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vistage.Services.Payments;
using Vistage.Services.Payments.Nodus;

namespace NodusTest
{
    [TestClass]
    public class UnitTest1
    {
        private BillingData _billingData;
        private const string SetupId = "eStore Connection";

        [TestInitialize]
        public void TestInitialize()
        {
            _billingData = new BillingData()
            {
                FirstName = "John",
                LastName = "Doe",
                Amount = "999.99",
                BillingAddress1 = "1234 West Main Street.",
                BillingCity = "San Diego",
                BillingState = "CA",
                BillingZip = "92130",
                Email = "mark.bidar@vistage.com",
                BillingCountryCode = "US",
                CCExpirationData = "1019",
                WorkPhone = "(555) 555-5555",
                MobilePhone = "(555) 555-5555",
                PhoneExtension = "0000",
                AccountName = "John Doe",
                FailOnAddressMismatch = "0",
                
            };
        }

        [TestMethod]
        public void TestCreditCard()
        {
            int refResult = 0;
            string errString = String.Empty;

            _billingData.CustomerCode = "MEMAPP000000141";
            _billingData.CreditCardNumber = "4111 1111 1111 1111";
            _billingData.CCType = CcType.Visa;
            _billingData.PaymentMethod = PaymentMethod.CreditCard;
            Assert.IsTrue(new NodusPaymentProcessor().ProcessPayment( _billingData, out refResult, ref errString, "test", "") == TransactionStatus.Success);
        }

        [TestMethod]
        public void TestInvoicing()
        {
            int refResult = 0;
            string errString = String.Empty;

            _billingData.CustomerCode = "MEMAPP000000141";
            _billingData.PaymentMethod = PaymentMethod.Invoicing;
            ;
            Assert.IsTrue(new NodusPaymentProcessor().ProcessPayment( _billingData, out refResult, ref errString, "test", "") == TransactionStatus.Success);
        }


        [TestMethod]
        public void TestEft()
        {
            int refResult = 0;
            string errString = String.Empty;
            _billingData.CustomerCode = "MEMAPP000000141";

            _billingData.PaymentMethod = PaymentMethod.Eft;
            Assert.IsTrue(new NodusPaymentProcessor().ProcessPayment(_billingData, out refResult, ref errString, "test", "") == TransactionStatus.Success);
        }
    }


}
