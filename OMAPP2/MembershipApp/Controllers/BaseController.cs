﻿using System.Configuration;
using System.Web.Mvc;

namespace MembershipApp.Controllers
{
    public class BaseController : Controller
    {
        protected string MaestroUrl = ConfigurationManager.AppSettings["MaestroUrlMembershipUrl"];
        protected string MaestroUser = ConfigurationManager.AppSettings["MaestroUser"];
        protected string Password = ConfigurationManager.AppSettings["Password"];


        /// <summary>
        /// Called when an unhandled exception occurs in the action.
        /// </summary>
        /// <param name="filterContext">Information about the current request and action.</param>
        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            Globals.Logger.Error("An Exception Occured.", filterContext.Exception);
            // Redirect on error:
            filterContext.Result = RedirectToAction("Index", "Error");

            // OR set the result without redirection:
            //filterContext.Result = new ViewResult
            //{
            //    ViewName = "~/Views/Shared/Error.cshtml"
            //};
        }

        protected ActionResult ShowError(string msg)
        {
            Globals.Logger.Debug(msg);
            ViewData["Message"] = msg;
            return View("Error");
        }
    }
}