﻿using System;
using System.Configuration;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Autofac;
using Microsoft.Extensions.Configuration;
using Vistage.Membership.Dto;
using Vistage.Platform.Validation;
using Vistage.Services.Membership;
using Vistage.Services.Payments;
using Autofac.Configuration;
using Vistage.Platform.Http;

namespace MembershipApp.Controllers
{
    [RoutePrefix("api/membership")]
    public class MembershipController : ApiController
    {
        protected string MaestroMembershipUrl = ConfigurationManager.AppSettings["MaestroMembershipUrl"];
        protected string MaestroUser = ConfigurationManager.AppSettings["MaestroUser"];
        protected string Password = ConfigurationManager.AppSettings["Password"];
        private static IContainer Container { get; set; }
        private static IPaymentProcessor _paymentProcessor;


        // GET: api/Membership/5
        [HttpGet]
        [Route("Validate")]
        public async Task<IHttpActionResult> Validate([FromUri]string token)
        {
            if (String.IsNullOrEmpty(token))
            {
                return BadRequest("Missing Token!");
            }

            Globals.Logger.InfoFormat("Getting application with token Id {0}", token);
            var client = new MembershipService(MaestroMembershipUrl, MaestroUser, Password, null);
            var result = await client.ValidateApplication(token);

            if (result.Data == null)
            {
                Globals.Logger.InfoFormat("Unable to get application for the given token with message {0}", result.Message);
                return BadRequest("No Membership Application Found For Token: " + token);
            }

            return Ok(result.Data);
        }

        // GET: api/Membership/5
        [HttpGet]
        [Route("Get")]
        public async Task<IHttpActionResult> Get([FromUri]string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return BadRequest("Missing Token!");
            }

            Globals.Logger.InfoFormat("Getting application with token Id {0}", id);
            var client = new MembershipService(MaestroMembershipUrl, MaestroUser, Password, null);
            var result = await client.GetApplication(id);

            if (result.Data == null)
            {
                Globals.Logger.InfoFormat("Unable to get application for the given token with message {0}", result.Message);
                return BadRequest("No Membership Application Found For Id: " + id);
            }

            return Ok(result.Data);
        }

        // POST: api/Membership
        [HttpPatch]
        public async Task<IHttpActionResult> Update([FromUri] string id, [FromBody] UpdateMembershipRequestDto request)
        {
            if (request == null)
            {
                return BadRequest("Missing data!");
            }

            string err;

            bool validated = JsonSchemaValidator.Validate(request, System.Web.Hosting.HostingEnvironment.MapPath("~/Schemas/PatchApplicationRequestSchema.json"), out err);
            if (validated)
            {
                var client = new MembershipService(MaestroMembershipUrl, MaestroUser, Password, null);
                var result = await client.SaveApplication(id, request);

                if (result.HttpResult != HttpStatusCode.OK)
                {
                    return BadRequest(result.Message);
                }

                return Ok(result.Data);
            }
            return BadRequest("Request does not pass schema validation");
        }

        [HttpPost]
        [Route("payment")]
        public IHttpActionResult ProcessPayments(BillingData billingData)
        {
            //var serObj = JsonConvert.SerializeObject(billingData, Formatting.Indented);
            var config = new ConfigurationBuilder();
            config.AddJsonFile("PaymentSettings.json");
            var configuration = config.Build();

            var environment = configuration["environment"];
            var mode = configuration["processingMode"];

            var module = new ConfigurationModule(configuration);
            var builder = new ContainerBuilder();
            builder.RegisterModule(module);

            Container = builder.Build();


            var paymmentResult = new PaymentResult();


            using (var scope = Container.BeginLifetimeScope())
            {
                _paymentProcessor = scope.Resolve<IPaymentProcessor>();

                string err;
                if (!_paymentProcessor.ValidateSchema(billingData, out err))
                {
                    paymmentResult.resultText = err;
                    paymmentResult.ccProcessingSuccess = false;
                    paymmentResult.ccAuthorizationResponsesCode = (int)PaymentProcessingStatus.Failed;
                    return Content(HttpStatusCode.BadRequest, paymmentResult);
                }

                int refResult;
                string responseString = String.Empty;

                var result = _paymentProcessor.ProcessPayment(billingData, out refResult, ref responseString, mode, environment);

                if (result == TransactionStatus.Failed)
                {

                    var logText = $"Error processing payment. Error={responseString}, Customer Code={billingData.CustomerCode}";
                    var errorText = $"Error processing payment. {responseString}";
                    Globals.Logger.Error(logText);
                    paymmentResult.resultText = errorText;
                    
                    paymmentResult.ccProcessingSuccess = false;
                    paymmentResult.ccAuthorizationResponsesCode = (int)PaymentProcessingStatus.Failed;
                    return Content(HttpStatusCode.BadRequest, paymmentResult);
                }

                paymmentResult.resultText = responseString; // This will say "Approved"
                paymmentResult.ccProcessingSuccess = true;
                paymmentResult.ccAuthorizationResponsesCode = (int) PaymentProcessingStatus.Approved;
                var text = $"Processing payment was successful. Customer Code={billingData.CustomerCode}";
                Globals.Logger.Info(text);
                return Content(HttpStatusCode.OK, paymmentResult);
            }
        }




        // PUT: api/Membership/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Membership/5
        public void Delete(int id)
        {
        }

    }
}
