﻿using System.Configuration;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Vistage.Membership.Dto;
using Vistage.Services.Geo;

namespace MembershipApp.Controllers
{
    [RoutePrefix("api/geo")]
    public class GeoController : ApiController
    {
        protected string MaestroGeoUrl = ConfigurationManager.AppSettings["MaestroGeoUrl"];
        protected string MaestroUser = ConfigurationManager.AppSettings["MaestroUser"];
        protected string Password = ConfigurationManager.AppSettings["Password"];

        /// <summary>
        /// Get list of countries
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(CountriesDto))]
        [HttpGet]
        public async Task<IHttpActionResult> Countries()
        {
            var client = new GeoService(MaestroGeoUrl, MaestroUser, Password);
            var result = await client.GetCountries();
            if (result.Data == null)
            {
                Globals.Logger.InfoFormat("Unable to get countries. Error: {0}", result.Message);
                return InternalServerError();
            }

            return Ok(result.Data);
        }

        [ResponseType(typeof(StatesDto))]
        [HttpGet]
        [Route("States/{id}")]
        public async Task<IHttpActionResult> States(string id)
        {
            var client = new GeoService(MaestroGeoUrl, MaestroUser, Password);
            var result = await client.GetStates(id);
            if (result.Data == null)
            {
                Globals.Logger.InfoFormat("Unable to get states. Error: {0}", result.Message);
                return InternalServerError();
            }

            return Ok(result.Data);
        }

    }
}
