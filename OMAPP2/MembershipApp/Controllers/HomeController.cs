﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Vistage.Membership.Dto;
using Newtonsoft.Json.Schema;
using Vistage.Platform.Http;
using Vistage.Platform.Validation;
using Vistage.Services.Membership;

namespace MembershipApp.Controllers
{
    //    some tokens for testing:
    //4cor1uvqwl9mn2caiz39jatcat    -> appId=22
    //4q1jloufhqd2zpmslkaafytnaq    -> appId=20
    //6x22sbe9314ix08api7hqiss3o    -> appId=21
    //7azi0kozet7vfxd8owtcazoaik    -> appId=23



    [RoutePrefix("membership")]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }


        public ActionResult MembershipCommitment()
        {
            System.Diagnostics.Debug.WriteLine("Membership Action");
            return View("MembershipCommitment");
        }

        public ActionResult MemberInformation()
        {
            System.Diagnostics.Debug.WriteLine("Member Action");
            return View("MemberInformation");
        }

        public ActionResult PaymentInformation()
        {
            System.Diagnostics.Debug.WriteLine("Payment Action");
            return View("PaymentInformation");
        }
        /// <summary>
        /// Validates the specified  token.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [Route("validate/{id}")]
        [HttpGet]
        [HandleError]
        public async System.Threading.Tasks.Task<ActionResult> Validate(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return ShowError("Missing Token.");
            }

            var service = new MembershipService(MaestroUrl, MaestroUser, Password, null);


            var result = await service.ValidateApplication(id);
            if (result.Data == null)
            {
                return ShowError("No Membership Application Found For Token: " + id);
            }

            var application = result.Data;
            result = await service.GetApplication(application.membershipApplicationId.ToString());

            if (result.IsSuccessful)
                return View("MembershipCommitment", result.Data.person);

            return ShowError(result.Message);
        }
     
        /// <summary>
        /// Edits the specified membership application identifier.
        /// </summary>
        /// <param name="membershipAppId">The membership application identifier.</param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<ActionResult> Edit(string membershipAppId)
        {
            if (String.IsNullOrEmpty(membershipAppId))
                return new EmptyResult();

            var result = await Rest.Get<MembershipApplication>(MaestroUrl, "" + membershipAppId);
            return View(result.Data.person);
        }

        /// <summary>
        /// Saves the specified application.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [Route("save/{id}")]
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> Save(string id, UpdateMembershipRequestDto request)
        {
            if (request == null)
            {
                return ShowError("Requst cannot be null!");
            }


            string err;

            bool validated = JsonSchemaValidator.Validate(request, HttpContext.Server.MapPath("~/Schemas/PatchApplicationRequestSchema.json"), out err);
            if (validated)
            {
                var client = new MembershipService(MaestroUrl, MaestroUser, Password, null);
                var result = await client.SaveApplication(id, request);

                if (result.HttpResult != HttpStatusCode.OK)
                {
                    return ShowError(result.Message);
                }
                // Depending on the step we should move on to the next one.
                return View("Edit", result.Data.person);
            }
            return ShowError(err);
        }


    }
}