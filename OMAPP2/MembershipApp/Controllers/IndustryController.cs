﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Vistage.Membership.Dto;
using Vistage.Services.Naics;

namespace MembershipApp.Controllers
{
    [RoutePrefix("api/Industry")]
    public class IndustryController : ApiController
    {
        protected string MaestroNaicsUrl = ConfigurationManager.AppSettings["MaestroNaicsUrl"];
        protected string MaestroUser = ConfigurationManager.AppSettings["MaestroUser"];
        protected string Password = ConfigurationManager.AppSettings["Password"];
   

        [ResponseType(typeof(IndustryDetailDto))]
        [HttpGet]
        
        public async Task<IHttpActionResult> Get(string id)
        {
            var client = new NaicsService(MaestroNaicsUrl, MaestroUser, Password);
            var result = await client.GetIndustry(id);
            if (result.Data == null)
            {
                Globals.Logger.InfoFormat("Unable to get industries. Error: {0}", result.Message);
                return InternalServerError();
            }

            return Ok(result.Data);

        }


        [ResponseType(typeof(IndustriesDto))]
        [HttpGet]
        public async Task<IHttpActionResult> Get()
        {
            var client = new NaicsService(MaestroNaicsUrl, MaestroUser, Password);
            var result = await client.GetIndustries();
            if (result.Data == null)
            {
                Globals.Logger.InfoFormat("Unable to get industries. Error: {0}", result.Message);
                return InternalServerError();
            }

            return Ok(result.Data);

        }
    }
}
