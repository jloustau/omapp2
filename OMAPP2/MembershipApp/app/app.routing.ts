﻿import { Routes, RouterModule, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { MembershipCommitmentModule } from './membership-commitment/membership-commitment.module';
import { MemberInformationModule } from './member-information/member-information.module';
import { PaymentInformationModule } from './payment-information/payment-information.module';
import { HomeModule } from './home/home.module';
import { InitialResolve } from './services/resolvers/initial-resolve.service';


const appRoutes: Routes = [
    {
        path: 'membership-terms',
        loadChildren: 'app/membership-commitment/membership-commitment.module#MembershipCommitmentModule'
    },
    {
        path: 'member-information',
        loadChildren: 'app/member-information/member-information.module#MemberInformationModule'
    },

    {
        path: 'payment-information',
        loadChildren: 'app/payment-information/payment-information.module#PaymentInformationModule'
    },
    {
        path: ':token',
        loadChildren: 'app/home/home.module#HomeModule'
    },
    {
        path: '',
        redirectTo: 'membership-terms',
        pathMatch: 'full'
    }
];

export const appRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);
