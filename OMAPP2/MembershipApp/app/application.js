"use strict";
exports.APP = {
    stepOneSaved: true,
    termsAccepted: true,
    termsAcceptedTimestamp: "",
    termsVersion: "NF-V10-7272015-Online",
    termsFilePath: "NF-V10-7272015-Online.html",
    stepTwoSaved: false,
    person: {
        prefix: "Mr",
        first: "John",
        middleInitial: "g",
        last: "Smith",
        suffix: "jr.",
        preferredName: "Johnny Cash",
        jobTitle: "CEO",
        workPhone: "",
        mobilePhone: "(619)555-1212",
        email: "superceo@vistage.com",
        birthday: "01/10/1977",
        gender: "M",
        spouse: "Marge Simpson",
        referralType: "abcd",
        referralName: "Bart Simpson"
    },
    company: {
        name: "Moes Tavern",
        businessDescription: "Provide a place to relax and have fun to the population of Springfield",
        typeOfOrg: "private",
        annualRevenue: "1 - 4 Million",
        numberOfEmployee: "100 - 499",
        industryNAICS: "1142",
        address1: "27 Evergreen Terrace",
        address2: "4567",
        city: "",
        state: "",
        zip: "09876",
        country: "United States",
        website: "http://thesimpsons.com"
    },
    stepThreeSaved: false,
    methodOfPayment: "",
    billingFrequency: "",
    billingInfo: {
        billingContactName: "",
        companyName: "Moes Tavern",
        address1: "27 Evergreen Terrace",
        address2: "4567",
        city: "Springfield",
        state: "",
        zip: "09876",
        country: "United States"
    },
    billingRateOption: [
        {
            frequency: "Annually",
            label: "$14,070 annually (save $430)",
            amount: "14070.00",
            methodOfPayment: [
                "CC",
                "EFT",
                "INV"
            ]
        },
        {
            frequency: "Semi-Annually",
            label: "$7,115 semi-annually (save $290)",
            amount: "7115.00",
            methodOfPayment: [
                "CC",
                "EFT",
                "INV"
            ]
        },
        {
            frequency: "Quarterly",
            label: "$3,630 quarterly",
            amount: "3630.00",
            methodOfPayment: [
                "CC",
                "EFT"
            ]
        },
        {
            frequency: "Monthly",
            label: "$1,210 monthly",
            amount: "1210.00",
            methodOfPayment: [
                "CC",
                "EFT"
            ]
        }
    ],
    appliedDiscounts: [
        {
            id: 1234,
            name: "0$ Enrollment - Gift of Vistage",
            type: "Monetary-One Time",
            amount: "2250.00"
        },
        {
            id: 1235,
            name: "10% Discount - Former Member",
            type: "Percentage-Monthly",
            percent: "10"
        }
    ],
    enrollmentFee: "2250",
    financialTermsAccepted: true,
    financialTermsAcceptanceName: "Homer G. Simpson",
    submitted: true,
    vistageProgram: "TA",
    customerCode: "",
    startDate: "01/01/2017"
};
//# sourceMappingURL=application.js.map