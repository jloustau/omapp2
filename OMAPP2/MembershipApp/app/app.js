"use strict";
var router_1 = require('@angular/router');
var appRoutes = [
    { path: 'membership-commitment', component: MembershipCommitmentComponent },
    { path: 'member-information', component: MemberInformationComponent },
    { path: 'payment-information', component: PaymentInformationComponent }
];
exports.appRoutingProviders = [];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.js.map