"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var ApplicationService = (function () {
    function ApplicationService() {
        this.app = {
            stepOneSaved: true,
            termsAccepted: true,
            termsVersion: "NF-V10-7272015-Online",
            termsFilePath: "NF-V10-7272015-Online.html",
            stepTwoSaved: true,
            person: {
                prefix: "Mr",
                first: "John",
                middleInitial: "g",
                last: "Smith",
                suffix: "jr.",
                preferredName: "Johnny Cash",
                jobTitle: "CEO",
                workPhone: "(619)555-1212",
                mobilePhone: "(619)555-1212",
                email: "superceo@vistage.com",
                birthday: "01/10/1977",
                gender: "M",
                spouse: "Marge Simpson",
                referralType: "abcd",
                referralName: "Bart Simpson"
            },
            company: {
                name: "Moes Tavern",
                businessDescription: "Provide a place to relax and have fun to the population of Springfield",
                typeOfOrg: "Company",
                annualRevenue: "1 - 4 Million",
                numberOfEmployee: "100 - 499",
                industryNAICS: "1142",
                address1: "27 Evergreen Terrace",
                address2: "4567",
                city: "Springfield",
                state: "MA",
                zip: "09876",
                country: "United States",
                website: "http://thesimpsons.com"
            },
            stepThreeSaved: false,
            methodOfPayment: "CC",
            billingFrequency: "Yearly",
            billingInfo: {
                companyName: "Moes Tavern",
                address1: "27 Evergreen Terrace",
                address2: "4567",
                city: "Springfield",
                state: "MA",
                zip: "09876",
                country: "United States"
            },
            financialTermsAccepted: true,
            financialTermsAcceptanceName: "Homer G. Simpson",
            submitted: true };
    }
    ApplicationService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], ApplicationService);
    return ApplicationService;
}());
exports.ApplicationService = ApplicationService;
//# sourceMappingURL=application.service.js.map