"use strict";
exports.APP = {
    stepOneSaved: false,
    termsAccepted: false,
    termsAcceptedTimestamp: "",
    termsVersion: "NF-V10-7272015-Online",
    termsFilePath: "NF-V10-7272015-Online.html",
    stepTwoSaved: false,
    person: {
        prefix: "",
        first: "",
        middleInitial: "",
        last: "",
        suffix: "",
        preferredName: "",
        jobTitle: "",
        workPhone: "",
        mobilePhone: "",
        email: "",
        birthday: "",
        gender: "",
        spouse: "",
        referralType: "",
        referralName: ""
    },
    company: {
        name: "",
        businessDescription: "",
        typeOfOrg: "",
        annualRevenue: "",
        numberOfEmployee: "",
        industryNAICS: "",
        address1: "",
        address2: "",
        city: "",
        state: "",
        zip: "",
        country: "",
        website: ""
    },
    stepThreeSaved: false,
    methodOfPayment: "",
    billingFrequency: "",
    billingInfo: {
        billingContactName: "",
        companyName: "",
        address1: "",
        address2: "",
        city: "",
        state: "",
        zip: "",
        country: ""
    },
    billingRateOption: [
        {
            frequency: "Annually",
            label: "$14,070 annually (save $430)",
            amount: "14070.00",
            methodOfPayment: [
                "CC",
                "EFT",
                "INV"
            ]
        },
        {
            frequency: "Semi-Annually",
            label: "$7,115 semi-annually (save $290)",
            amount: "7115.00",
            methodOfPayment: [
                "CC",
                "EFT",
                "INV"
            ]
        },
        {
            frequency: "Quarterly",
            label: "$3,630 quarterly",
            amount: "3630.00",
            methodOfPayment: [
                "CC",
                "EFT"
            ]
        },
        {
            frequency: "Monthly",
            label: "$1,210 monthly",
            amount: "1210.00",
            methodOfPayment: [
                "CC",
                "EFT"
            ]
        }
    ],
    appliedDiscounts: [
        {
            id: 1234,
            name: "0$ Enrollment - Gift of Vistage",
            type: "Monetary-One Time",
            amount: "2250.00"
        },
        {
            id: 1235,
            name: "10% Discount - Former Member",
            type: "Percentage-Monthly",
            percent: "10"
        }
    ],
    enrollmentFee: "2250",
    financialTermsAccepted: false,
    financialTermsAcceptanceName: "",
    submitted: false,
    vistageProgram: "CE",
    customerCode: "",
    startDate: "01/01/2017"
};
//# sourceMappingURL=emptyApplication.js.map