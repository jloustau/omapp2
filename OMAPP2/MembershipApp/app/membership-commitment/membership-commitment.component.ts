﻿import { Application } from '../application.interface'
import { ApplicationService } from '../services/application.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'Home/MembershipCommitment',
    providers: [ApplicationService]
})

export class MembershipCommitmentComponent implements OnInit {

    /* init private variables */
    private app: Application;
    private isCheckboxChecked: boolean;

    /* constructor */
    constructor(private fb: FormBuilder, public router: Router, private route: ActivatedRoute, private _applicationService: ApplicationService) {
        this.route.data.subscribe(
            data => { console.log(data) },
            error => console.log(error)
        );
    };

    /* onInit */
    ngOnInit() {
        this.app = this.route.snapshot.data['application'] as Application;
        this.isCheckboxChecked = this.app.termsAccepted;
        console.log("this app");
        console.log(this.app);
    }

    /* onSubmit */
    onSubmit(membershipForm) {
        if (this.app.termsAccepted === true) {
            this.app.stepOneSaved = true;
            this.updateMembership(this.app);
            this.router.navigate(['member-information', { membershipId: this.app.membershipApplicationId }]);

        } else {
            console.log('form is invalid');
            membershipForm.tested = true;
        }
    }

    updateCheckbox(checked: boolean) {
        this.isCheckboxChecked = checked;
        if (!this.isCheckboxChecked) {
            this.app.stepOneSaved = false;
            this.app.stepTwoSaved = false;
        }
    }

    updateMembership(app: Application) {
        console.log('updateMembership()');
        this._applicationService.updateMembership(app).subscribe(
            error => console.log(error),
            () => console.log('updateSuccessful')
        );
    }

    get diagnostics() { return JSON.stringify(this.app, ['stepOneSaved', 'termsAccepted', 'termsAcceptedTimestamp']) }

}