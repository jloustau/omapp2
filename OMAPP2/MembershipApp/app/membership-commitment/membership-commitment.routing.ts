﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MembershipCommitmentComponent } from './membership-commitment.component'
import { MembershipCommitmentResolve } from '../services/resolvers/membership-commitment-resolve.service';



const membershipCommitmentRoutes: Routes = [
    {
        path: '',
        component: MembershipCommitmentComponent,
        resolve: {
            application: MembershipCommitmentResolve
        }
    }
];

export const membershipCommitmentRouting: ModuleWithProviders = RouterModule.forChild(membershipCommitmentRoutes);