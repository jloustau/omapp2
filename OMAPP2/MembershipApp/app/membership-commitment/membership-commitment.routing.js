"use strict";
var router_1 = require('@angular/router');
var membership_commitment_component_1 = require('./membership-commitment.component');
var membership_commitment_resolve_service_1 = require('../services/resolvers/membership-commitment-resolve.service');
var membershipCommitmentRoutes = [
    {
        path: '',
        component: membership_commitment_component_1.MembershipCommitmentComponent,
        resolve: {
            application: membership_commitment_resolve_service_1.MembershipCommitmentResolve
        }
    }
];
exports.membershipCommitmentRouting = router_1.RouterModule.forChild(membershipCommitmentRoutes);
//# sourceMappingURL=membership-commitment.routing.js.map