"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var application_service_1 = require('../services/application.service');
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var MembershipCommitmentComponent = (function () {
    /* constructor */
    function MembershipCommitmentComponent(fb, router, route, _applicationService) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this._applicationService = _applicationService;
        this.route.data.subscribe(function (data) { console.log(data); }, function (error) { return console.log(error); });
    }
    ;
    /* onInit */
    MembershipCommitmentComponent.prototype.ngOnInit = function () {
        this.app = this.route.snapshot.data['application'];
        this.isCheckboxChecked = this.app.termsAccepted;
        console.log("this app");
        console.log(this.app);
    };
    /* onSubmit */
    MembershipCommitmentComponent.prototype.onSubmit = function (membershipForm) {
        if (this.app.termsAccepted === true) {
            this.app.stepOneSaved = true;
            this.updateMembership(this.app);
            this.router.navigate(['member-information', { membershipId: this.app.membershipApplicationId }]);
        }
        else {
            console.log('form is invalid');
            membershipForm.tested = true;
        }
    };
    MembershipCommitmentComponent.prototype.updateCheckbox = function (checked) {
        this.isCheckboxChecked = checked;
        if (!this.isCheckboxChecked) {
            this.app.stepOneSaved = false;
            this.app.stepTwoSaved = false;
        }
    };
    MembershipCommitmentComponent.prototype.updateMembership = function (app) {
        console.log('updateMembership()');
        this._applicationService.updateMembership(app).subscribe(function (error) { return console.log(error); }, function () { return console.log('updateSuccessful'); });
    };
    Object.defineProperty(MembershipCommitmentComponent.prototype, "diagnostics", {
        get: function () { return JSON.stringify(this.app, ['stepOneSaved', 'termsAccepted', 'termsAcceptedTimestamp']); },
        enumerable: true,
        configurable: true
    });
    MembershipCommitmentComponent = __decorate([
        core_1.Component({
            templateUrl: 'Home/MembershipCommitment',
            providers: [application_service_1.ApplicationService]
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, router_1.Router, router_1.ActivatedRoute, application_service_1.ApplicationService])
    ], MembershipCommitmentComponent);
    return MembershipCommitmentComponent;
}());
exports.MembershipCommitmentComponent = MembershipCommitmentComponent;
//# sourceMappingURL=membership-commitment.component.js.map