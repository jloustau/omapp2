﻿import { NgModule }  from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FormGroup, FormBuilder, Validators }  from '@angular/forms';
import { MembershipCommitmentComponent } from './membership-commitment.component'
import { membershipCommitmentRouting } from './membership-commitment.routing';
import { MembershipCommitmentResolve } from '../services/resolvers/membership-commitment-resolve.service';

@NgModule({

    declarations: [MembershipCommitmentComponent],
    imports: [CommonModule, FormsModule, ReactiveFormsModule, membershipCommitmentRouting],
    exports: [MembershipCommitmentComponent],
    providers: [MembershipCommitmentResolve]
})

export class MembershipCommitmentModule {
}