"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var forms_1 = require('@angular/forms');
var membership_commitment_component_1 = require('./membership-commitment.component');
var membership_commitment_routing_1 = require('./membership-commitment.routing');
var membership_commitment_resolve_service_1 = require('../services/resolvers/membership-commitment-resolve.service');
var MembershipCommitmentModule = (function () {
    function MembershipCommitmentModule() {
    }
    MembershipCommitmentModule = __decorate([
        core_1.NgModule({
            declarations: [membership_commitment_component_1.MembershipCommitmentComponent],
            imports: [common_1.CommonModule, forms_1.FormsModule, forms_1.ReactiveFormsModule, membership_commitment_routing_1.membershipCommitmentRouting],
            exports: [membership_commitment_component_1.MembershipCommitmentComponent],
            providers: [membership_commitment_resolve_service_1.MembershipCommitmentResolve]
        }), 
        __metadata('design:paramtypes', [])
    ], MembershipCommitmentModule);
    return MembershipCommitmentModule;
}());
exports.MembershipCommitmentModule = MembershipCommitmentModule;
//# sourceMappingURL=membership-commitment.module.js.map