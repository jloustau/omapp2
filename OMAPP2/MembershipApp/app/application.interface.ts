﻿export interface Application {

    membershipApplicationId?: number;
    stepOneSaved: boolean;
    termsAccepted: boolean;
    termsAcceptedTimestamp: string;
    termsVersion: string;
    termsFilePath: string;
    stepTwoSaved: boolean;
    person: {

        prefix: string;
        first: string;
        middleInitial: string;
        last: string;
        suffix: string
        preferredName: string;
        jobTitle: string;
        workPhone: string;
        mobilePhone: string;
        email: string;
        birthday: string;
        gender: string;
        spouse: string;
        referralType: string;
        referralName: string
    };

    company: {

        name: string;
        businessDescription: string;
        typeOfOrg: string;
        annualRevenue: string;
        numberOfEmployee: string;
        industryNAICS: string;
        address1: string;
        address2: string;
        city: string;
        state: string;
        zip: string;
        country: string;
        website: string
    };

    stepThreeSaved: boolean;
    methodOfPayment: string;
    billingFrequency: string;
    billingInfo: {

        billingContactName: string;
        companyName: string;
        address1: string;
        address2: string;
        city: string;
        state: string;
        zip: string;
        country: string

    };
    appliedDiscounts?: Array<Object>;
    billingRateOption?: Array<Object>;
    enrollmentFee: string;
    financialTermsAccepted: boolean;
    financialTermsAcceptanceName: string;
    thankYouPaymentType?: string;
    submitted: boolean;
    vistageProgram: string;
    customerCode: string;
    startDate: string;

}