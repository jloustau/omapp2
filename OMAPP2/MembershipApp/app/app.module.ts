import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { appRouting } from './app.routing';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent }  from './app.component';
import { HttpModule } from '@angular/http';
import { ApplicationService } from './services/application.service';

import 'rxjs/add/operator/map';


@NgModule({
    imports: [  BrowserModule,
        ReactiveFormsModule,
        appRouting, HttpModule],
    declarations: [AppComponent],
    providers: [ ApplicationService],
    bootstrap: [ AppComponent]
})
export class AppModule { }
