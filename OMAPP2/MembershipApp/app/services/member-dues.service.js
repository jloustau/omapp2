/*** ./app/services/member-dues.service.ts ***/
/** -------DEPRECATED-------**/
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Imports
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
// Import RxJs required methods
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
var MemberDuesService = (function () {
    // Resolve HTTP using the constructor
    function MemberDuesService(http) {
        this.http = http;
        this.headers = new http_1.Headers({
            'accept': 'application/json',
            'accept-language': 'en-US, en; q = 0.8',
            'Content-Type': 'application/json'
        });
        this.options = new http_1.RequestOptions({ headers: this.headers });
    }
    MemberDuesService.prototype.getMemberDues = function () {
        console.log('in getApp()');
        var result = this.http
            .get('app/memberDues.json', this.options)
            .map(function (res) { return res.json(); });
        return result;
    };
    MemberDuesService.prototype.getNMMemberDues = function () {
        var result = this.http
            .get('app/newMexicoMemberDues.json', this.options)
            .map(function (res) { return res.json(); });
        return result;
    };
    MemberDuesService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], MemberDuesService);
    return MemberDuesService;
}());
exports.MemberDuesService = MemberDuesService;
//# sourceMappingURL=member-dues.service.js.map