﻿/*** ./app/services/data.service.ts ***/

// Imports
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DataService {

    // Resolve HTTP using the constructor
    constructor(private http: Http) { }

    // private instance variable to cold base url
    private countriesUrl = 'https://restcountries.herokuapp.com/api/v1';
    private headers = new Headers({
        'accept': 'application/json',
        'accept-language': 'en-US, en; q = 0.8',
        'Content-Type': 'application/json'
    });
    private options = new RequestOptions({ headers: this.headers });


    // Fetch all existing countries
    getCountries() {
        var result = this.http.get('api/geo/countires', this.options).map((res: Response) => res.json());
        return result;
    }

    getStates(countryCode: string) {
        var result = this.http.get('api/geo/states/' + countryCode, this.options).map((res: Response) => res.json());
        return result;
    }

    getIndustries() {
        var result = this.http.get('api/industry', this.options).map((res: Response) => res.json());
        return result;
    }

    getIndustryParent(industryDetail: string) {
        var result = this.http.get('api/industry' + industryDetail, this.options).map((res: Response) => res.json());
        return result;
    }

    /**
    getBillingRateOption() {
        console.log('in getBillingRateOption()');
        var result = this.http.get('app/billingOption.json').map((res: Response) => res.json());
        return result;
    }
    **/
    

    getMembershipApp() {
        var result = this.http.get('api/membership/a84dmhcavmgbgt3blawsq8ovmx', this.options).map((res: Response) => res.json());
        return result;
    }
}