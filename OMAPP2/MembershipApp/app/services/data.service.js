/*** ./app/services/data.service.ts ***/
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// Imports
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
// Import RxJs required methods
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
var DataService = (function () {
    // Resolve HTTP using the constructor
    function DataService(http) {
        this.http = http;
        // private instance variable to cold base url
        this.countriesUrl = 'https://restcountries.herokuapp.com/api/v1';
        this.headers = new http_1.Headers({
            'accept': 'application/json',
            'accept-language': 'en-US, en; q = 0.8',
            'Content-Type': 'application/json'
        });
        this.options = new http_1.RequestOptions({ headers: this.headers });
    }
    // Fetch all existing countries
    DataService.prototype.getCountries = function () {
        var result = this.http.get('api/geo/countires', this.options).map(function (res) { return res.json(); });
        return result;
    };
    DataService.prototype.getStates = function (countryCode) {
        var result = this.http.get('api/geo/states/' + countryCode, this.options).map(function (res) { return res.json(); });
        return result;
    };
    DataService.prototype.getIndustries = function () {
        var result = this.http.get('api/industry', this.options).map(function (res) { return res.json(); });
        return result;
    };
    DataService.prototype.getIndustryParent = function (industryDetail) {
        var result = this.http.get('api/industry' + industryDetail, this.options).map(function (res) { return res.json(); });
        return result;
    };
    /**
    getBillingRateOption() {
        console.log('in getBillingRateOption()');
        var result = this.http.get('app/billingOption.json').map((res: Response) => res.json());
        return result;
    }
    **/
    DataService.prototype.getMembershipApp = function () {
        var result = this.http.get('api/membership/a84dmhcavmgbgt3blawsq8ovmx', this.options).map(function (res) { return res.json(); });
        return result;
    };
    DataService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;
//# sourceMappingURL=data.service.js.map