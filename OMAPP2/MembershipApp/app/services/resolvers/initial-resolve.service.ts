﻿import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Application } from '../../application.interface';
import { ApplicationService } from '../application.service';


@Injectable()
export class InitialResolve implements Resolve<Application> {

    constructor(private service: ApplicationService, private router: Router) { }

    resolve(route: ActivatedRouteSnapshot) {
        let token = route.params['token']

        return this.service.validateMembershipApp(token).then(application => {
            if (application && application.stepOneSaved === false) {

                this.router.navigate(['membership-terms', { membershipId: application.membershipApplicationId }]);
                return true;
            }

            if (application && application.stepOneSaved === true && application.stepTwoSaved === false) {
                this.router.navigate(['member-information', { membershipId: application.membershipApplicationId }]);
                return true;

            }

            if (application && application.stepOneSaved === true && application.stepTwoSaved === true) {
                this.router.navigate(['payment-information', { membershipId: application.membershipApplicationId }]);
                return true;
            }
            
        });
    }

}