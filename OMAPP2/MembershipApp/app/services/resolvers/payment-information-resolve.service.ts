﻿import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Application } from '../../application.interface';
import { ApplicationService } from '../application.service';


@Injectable()
export class PaymentInformationResolve implements Resolve<Application> {

    constructor(private service: ApplicationService) { }

    resolve(route: ActivatedRouteSnapshot) {
        console.log('inside payment-resolve');
        let membershipId = route.params['membershipId'];

        return this.service.getMembershipApp(membershipId).then(application => {
            if (application) {
                return application;
            } else {
                return false;
            }
        });
    }
}