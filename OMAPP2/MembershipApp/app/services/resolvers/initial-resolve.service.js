"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var application_service_1 = require('../application.service');
var InitialResolve = (function () {
    function InitialResolve(service, router) {
        this.service = service;
        this.router = router;
    }
    InitialResolve.prototype.resolve = function (route) {
        var _this = this;
        var token = route.params['token'];
        return this.service.validateMembershipApp(token).then(function (application) {
            if (application && application.stepOneSaved === false) {
                _this.router.navigate(['membership-terms', { membershipId: application.membershipApplicationId }]);
                return true;
            }
            if (application && application.stepOneSaved === true && application.stepTwoSaved === false) {
                _this.router.navigate(['member-information', { membershipId: application.membershipApplicationId }]);
                return true;
            }
            if (application && application.stepOneSaved === true && application.stepTwoSaved === true) {
                _this.router.navigate(['payment-information', { membershipId: application.membershipApplicationId }]);
                return true;
            }
        });
    };
    InitialResolve = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [application_service_1.ApplicationService, router_1.Router])
    ], InitialResolve);
    return InitialResolve;
}());
exports.InitialResolve = InitialResolve;
//# sourceMappingURL=initial-resolve.service.js.map