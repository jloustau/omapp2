﻿import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Application } from '../../application.interface';
import { ApplicationService } from '../application.service';


@Injectable()
export class MemberInformationResolve implements Resolve<Application> {

    constructor(private service: ApplicationService) { }

    resolve(route: ActivatedRouteSnapshot) {
        let membershipId = route.params['membershipId'];

        console.log('inside member-information-resolve');
        console.log(membershipId);
  
        return this.service.getMembershipApp(membershipId).then(application => {
            if (application) {
                return application;
            } else {
                return false;
            }
        });
    }

}