﻿/*** ./app/services/application.service.ts ***/

// Imports
import { Injectable } from '@angular/core';
import { Application } from '../application.interface';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class ApplicationService{

    // Resolve HTTP using the constructor
    constructor(private http: Http) { }

    private headers = new Headers({
        'accept': 'application/json',
        'accept-language': 'en-US, en; q = 0.8',
        'Content-Type': 'application/json'
    });

    private options = new RequestOptions({ headers: this.headers });

    validateMembershipApp(token: string) {
        console.log('in getApp()');
        var result = this.http
            .get('api/membership/Validate?token=' + token, this.options)
            .map((res: Response) => res.json()).toPromise();
        return result;
    }

    getMembershipApp(id: string) {
        console.log('in getApp()');
        var result = this.http
            .get('api/membership/Get?id=' + id, this.options)
            .map((res: Response) => res.json()).toPromise();
        return result;
    }

    processPayment(payment) {
        console.log('in processPayment()');
        var result = this.http
            .post('api/membership/payment', this.options)
            .map((res: Response) => res.json());
        return result;
    }

    updateMembership(app: Application) {
        console.log('in updateMembership()');
        var result = this.http
            .patch('api/membership/Update?id=' + app.membershipApplicationId, app, this.options)
            .map((res: Response) => res.json());
        return result;
    }

}