﻿/*** ./app/services/member-dues.service.ts ***/
/** -------DEPRECATED-------**/

// Imports
import { Injectable } from '@angular/core';
import { Application } from '../application.interface';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class MemberDuesService {

    // Resolve HTTP using the constructor
    constructor(private http: Http) { }

    private headers = new Headers({
        'accept': 'application/json',
        'accept-language': 'en-US, en; q = 0.8',
        'Content-Type': 'application/json'
    });

    private options = new RequestOptions({ headers: this.headers });

    getMemberDues() {
        console.log('in getApp()');
        var result = this.http
            .get('app/memberDues.json', this.options)
            .map((res: Response) => res.json());
        return result;
    }

    getNMMemberDues() {
        var result = this.http
            .get('app/newMexicoMemberDues.json', this.options)
            .map((res: Response) => res.json());
        return result;
    }
}