"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var forms_1 = require('@angular/forms');
var payment_information_component_1 = require('./payment-information.component');
var payment_information_routing_1 = require('./payment-information.routing');
var payment_information_resolve_service_1 = require('../services/resolvers/payment-information-resolve.service');
var capitalize_pipe_1 = require('../pipes/capitalize.pipe');
var displayPaymentMethodName_pipe_1 = require('../pipes/displayPaymentMethodName.pipe');
var displayProgramName_pipe_1 = require('../pipes/displayProgramName.pipe');
var PaymentInformationModule = (function () {
    function PaymentInformationModule() {
    }
    PaymentInformationModule = __decorate([
        core_1.NgModule({
            declarations: [payment_information_component_1.PaymentInformationComponent, capitalize_pipe_1.CapitalizePipe, displayPaymentMethodName_pipe_1.DisplayPaymentMethodNamePipe, displayProgramName_pipe_1.DisplayProgramNamePipe],
            imports: [common_1.CommonModule, forms_1.FormsModule, forms_1.ReactiveFormsModule, payment_information_routing_1.paymentInformationRouting],
            exports: [payment_information_component_1.PaymentInformationComponent],
            providers: [payment_information_resolve_service_1.PaymentInformationResolve]
        }), 
        __metadata('design:paramtypes', [])
    ], PaymentInformationModule);
    return PaymentInformationModule;
}());
exports.PaymentInformationModule = PaymentInformationModule;
//# sourceMappingURL=payment-information.module.js.map