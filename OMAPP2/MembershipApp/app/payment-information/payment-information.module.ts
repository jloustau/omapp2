﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PaymentInformationComponent } from './payment-information.component'
import { paymentInformationRouting } from './payment-information.routing';
import { PaymentInformationResolve } from '../services/resolvers/payment-information-resolve.service';
import { CapitalizePipe } from '../pipes/capitalize.pipe';
import { DisplayPaymentMethodNamePipe } from '../pipes/displayPaymentMethodName.pipe';
import { DisplayProgramNamePipe } from '../pipes/displayProgramName.pipe';



@NgModule({

    declarations: [ PaymentInformationComponent, CapitalizePipe, DisplayPaymentMethodNamePipe, DisplayProgramNamePipe ],
    imports: [ CommonModule, FormsModule, ReactiveFormsModule, paymentInformationRouting ],
    exports: [ PaymentInformationComponent ],
    providers: [ PaymentInformationResolve ]
})

export class PaymentInformationModule {
}