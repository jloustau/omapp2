"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var application_service_1 = require('../services/application.service');
var core_1 = require('@angular/core');
var data_service_1 = require('../services/data.service');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var PaymentInformationComponent = (function () {
    /* constructor */
    function PaymentInformationComponent(fb, router, _dataService, _applicationService, route) {
        this.fb = fb;
        this.router = router;
        this._dataService = _dataService;
        this._applicationService = _applicationService;
        this.route = route;
        this.billingFrequencyActive = true;
        this.currentYear = new Date().getFullYear();
        this.expirationMonth = "";
        this.expirationYear = "";
        this.payment = {};
        this.today = new Date();
        this.YEAR_RANGE = 10;
        this.years = [{ value: this.currentYear, display: this.currentYear }];
        this.discountsTotal = 0;
        /* standing data */
        this.cardTypes = [
            { value: 'VISA', display: 'Visa' },
            { value: 'MC', display: 'MasterCard' },
            { value: 'AMEX', display: 'American Express' }
        ];
        this.invoicingBillingFrequencies = [
            { value: 'annual', display: 'Annual' },
            { value: 'semi-annual', display: 'Semi-Annual' },
        ];
        this.months = [
            { value: '01', display: 'January' },
            { value: '02', display: 'February' },
            { value: '03', display: 'March' },
            { value: '04', display: 'April' },
            { value: '05', display: 'May' },
            { value: '06', display: 'June' },
            { value: '07', display: 'July' },
            { value: '08', display: 'August' },
            { value: '09', display: 'September' },
            { value: '10', display: 'October' },
            { value: '11', display: 'November' },
            { value: '12', display: 'December' }
        ];
        this.paymentMethods = [
            { value: 'CC', display: 'Recurring Credit Card' },
            { value: 'EFT', display: 'Recurring Electronic Funds Transfer' },
            { value: 'INV', display: 'Invoicing' }
        ];
        this.PAYMENT_METHOD_TYPE = {
            CARD: 'card',
            TRANSFER: 'transfer',
            INVOICING: 'invoicing'
        };
        this.thankYouPayments = [
            { value: 'DUES', display: 'Dues Offset' },
            { value: 'CASH', display: 'Cash' },
            { value: 'OPT', display: 'Opt Out' }
        ];
        this.route.data.subscribe(function (data) { console.log(data); }, function (error) { return console.log(error); });
    }
    ;
    ;
    /* OnInit */
    PaymentInformationComponent.prototype.ngOnInit = function () {
        this.app = this.route.snapshot.data['application'];
        var objDate = new Date(this.app.startDate), locale = "en-us", year = objDate.getFullYear(), month = objDate.toLocaleString(locale, { month: "long" });
        this.billingStartMonth = month + " " + year;
        this.billingFrequencies = this.app.billingRateOption;
        /* IE will set dropdowns to first value if value is null */
        this.app.methodOfPayment = this.app.methodOfPayment === null ? "" : this.app.methodOfPayment;
        this.app.billingFrequency = this.app.billingFrequency === null ? "" : this.app.billingFrequency;
        this.app.billingInfo.country = this.app.billingInfo.country === null ? "US" : this.app.billingInfo.country;
        this.app.billingInfo.state = this.app.billingInfo.state === null ? "" : this.app.billingInfo.state;
        this.payment.CCType = (this.payment.CCType === null || (!this.payment.CCType)) ? "" : this.payment.CCType;
        if (this.billingFrequencies.length > 0) {
            this.getFirstMonthDue();
        }
        ;
        this.initYearDropdown();
        this.initPaymentObject();
        this.getDiscounts();
        this.calculateTotal();
        this.getCountries();
    };
    /* OnSubmit */
    PaymentInformationComponent.prototype.onSubmit = function (paymentForm, payment) {
        this.updateBillingFields();
        if (paymentForm.valid) {
            console.log('form is valid');
            console.log(JSON.stringify(this.app));
            console.log('payment');
            console.log(JSON.stringify(this.payment));
            this.processPayment(payment);
        }
        else {
            console.log('form is invalid');
            paymentForm.tested = true;
        }
    };
    /* private methods */
    PaymentInformationComponent.prototype.calculateTotal = function () {
        this.totalAmount = Number(this.app.enrollmentFee) + Number(this.firstMonthDue) - this.discountsTotal;
    };
    PaymentInformationComponent.prototype.filterBillingRateOptions = function (paymentMethod) {
        this.availablebillingFrequencies = this.billingFrequencies.filter(function (frequency) { return frequency.methodOfPayment.indexOf(paymentMethod) > -1; });
    };
    PaymentInformationComponent.prototype.formatPaymentMethod = function (paymentMethod) {
        if (paymentMethod === 'CC') {
            return "Credit Card";
        }
        else if (paymentMethod === 'EFT') {
            return "Electronic fund transfer";
        }
        else if (paymentMethod === 'INV') {
            return "Invoicing";
        }
    };
    /**
      getBillingRateOptions() {
        this._dataService.getBillingRateOption().subscribe(
            data => {
                this.billingFrequencies = data.billingRateOption
                this.availablebillingFrequencies = this.billingFrequencies;
            },
            error => console.log(error),
            () => console.log('donde loading billingRateOptions')
        );
    }
    **/
    PaymentInformationComponent.prototype.getCountries = function () {
        var _this = this;
        this._dataService.getCountries().subscribe(function (data) { _this.countries = data.countries; }, function (error) { return console.log(error); }, function () { return _this.getStates(_this.app.billingInfo.country); });
    };
    PaymentInformationComponent.prototype.getCountryCode = function (countryName) {
        var country = this.countries.filter(function (country) { return country.name === countryName; });
        return country[0].countryCode;
    };
    PaymentInformationComponent.prototype.getDiscounts = function () {
        var appliedDiscounts = this.app.appliedDiscounts;
        for (var i = 0; i < appliedDiscounts.length; i++) {
            if (appliedDiscounts[i].type === "Percentage-Monthly") {
                appliedDiscounts[i].amount = (appliedDiscounts[i].percent * Number(this.firstMonthDue)) / 100;
            }
        }
        this.discounts = appliedDiscounts;
        this.getDiscountsTotal();
    };
    PaymentInformationComponent.prototype.getDiscountsTotal = function () {
        for (var i = 0; i < this.discounts.length; i++) {
            this.discountsTotal += Number(this.discounts[i].amount);
        }
    };
    PaymentInformationComponent.prototype.getFirstMonthDue = function () {
        var monthlyBillingRateOption = this.billingFrequencies.filter(function (item) { return item.frequency === "Monthly"; });
        this.firstMonthDue = monthlyBillingRateOption[0].amount;
    };
    /*getMemberDues(groupType: string) {
       this._memberDuesService.getMemberDues().subscribe(
           data => { this.memberDues = data[groupType][0] },
           error => console.log(error),
           () => this.getDiscounts()
       );
   }*/
    PaymentInformationComponent.prototype.getStates = function (countryCode) {
        var _this = this;
        this._dataService.getStates(countryCode).subscribe(function (data) { _this.states = data.states; }, function (error) { return console.log(error); });
    };
    PaymentInformationComponent.prototype.goBack = function () {
        this.router.navigate(['member-information', { membershipId: this.app.membershipApplicationId }]);
    };
    PaymentInformationComponent.prototype.initPaymentObject = function () {
        this.payment.FirstName = this.app.person.first;
        this.payment.LastName = this.app.person.last;
        this.payment.Address1 = this.app.company.address1;
        this.payment.Address2 = this.app.company.address2;
        this.payment.City = this.app.company.city;
        this.payment.State = this.app.company.state;
        this.payment.Zip = this.app.company.zip;
        this.payment.CountryCode = this.app.company.country;
        this.payment.Email = this.app.person.email;
        this.payment.MobilePhone = this.app.person.mobilePhone ? this.app.person.mobilePhone : "";
        this.payment.WorkPhone = this.app.person.workPhone;
        this.payment.CustomerCode = this.app.customerCode;
    };
    PaymentInformationComponent.prototype.initYearDropdown = function () {
        for (var i = 1; i < this.YEAR_RANGE + 1; i++) {
            this.years.push({ value: this.currentYear + i, display: this.currentYear + i });
        }
    };
    ;
    PaymentInformationComponent.prototype.processPayment = function (payment) {
        var _this = this;
        this._applicationService.processPayment(payment).subscribe(function (data) { _this.countries = data.countries; }, function (error) { return console.log(error); }, function () { return _this._applicationService.updateMembership(_this.app); });
    };
    PaymentInformationComponent.prototype.updateBillingFields = function () {
        this.payment.PaymentMethod = this.formatPaymentMethod(this.app.methodOfPayment);
        this.payment.CCType = this.payment.CCType;
        this.payment.BillingAddress1 = this.app.billingInfo.address1;
        this.payment.BillingAddress2 = this.app.billingInfo.address2;
        this.payment.BillingCity = this.app.billingInfo.city;
        this.payment.BillingState = this.app.billingInfo.state;
        this.payment.BillingCountryCode = this.app.billingInfo.country;
        this.payment.BillingZip = this.app.billingInfo.zip;
        this.payment.Amount = String((Number(this.app.enrollmentFee) + Number(this.firstMonthDue)) - this.discountsTotal);
        this.payment.AccountName = this.app.billingInfo.billingContactName ? this.app.billingInfo.billingContactName : (this.app.person.first + " " + this.app.person.last);
        this.payment.PhoneExtension = "";
    };
    PaymentInformationComponent.prototype.updateExpirationMonth = function (month) {
        this.payment.CCExpirationData = "";
        this.payment.CCExpirationData = month + this.expirationYear.substring(2, 4);
    };
    PaymentInformationComponent.prototype.updateExpirationYear = function (year) {
        this.payment.CCExpirationData = "";
        this.payment.CCExpirationData = this.expirationMonth + year.substring(2, 4);
    };
    PaymentInformationComponent.prototype.updateState = function (countryName) {
        this.app.company.state = "";
        var countryCode = this.getCountryCode(countryName);
        this.getStates(countryCode);
    };
    Object.defineProperty(PaymentInformationComponent.prototype, "diagnostics", {
        get: function () {
            var array = [JSON.stringify(this.app.billingInfo), JSON.stringify(this.app, ['stepTwoSaved', 'methodOfPayment', 'billingFrequency', 'financialTermsAccepted', 'financialTermsAcceptanceName', 'financialTermsTimestamp']), JSON.stringify(this.payment)];
            return array;
        },
        enumerable: true,
        configurable: true
    });
    PaymentInformationComponent = __decorate([
        core_1.Component({
            templateUrl: 'Home/PaymentInformation',
            providers: [application_service_1.ApplicationService, data_service_1.DataService]
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, router_1.Router, data_service_1.DataService, application_service_1.ApplicationService, router_1.ActivatedRoute])
    ], PaymentInformationComponent);
    return PaymentInformationComponent;
}());
exports.PaymentInformationComponent = PaymentInformationComponent;
//# sourceMappingURL=payment-information.component.js.map