﻿export interface Discount {
    id: number;
    name: string;
    type: string;
    percent?: number;
    amount?: number;
}