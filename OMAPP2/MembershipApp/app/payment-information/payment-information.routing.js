"use strict";
var router_1 = require('@angular/router');
var payment_information_component_1 = require('./payment-information.component');
var payment_information_resolve_service_1 = require('../services/resolvers/payment-information-resolve.service');
var paymentInformationRoutes = [
    {
        path: '',
        component: payment_information_component_1.PaymentInformationComponent,
        resolve: {
            application: payment_information_resolve_service_1.PaymentInformationResolve
        }
    }
];
exports.paymentInformationRouting = router_1.RouterModule.forChild(paymentInformationRoutes);
//# sourceMappingURL=payment-information.routing.js.map