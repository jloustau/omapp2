﻿import { Application } from '../application.interface';
import { ApplicationService } from '../services/application.service';
import { CapitalizePipe } from '../pipes/capitalize.pipe';
import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Discount } from './discount.interface';
import { DisplayPaymentMethodNamePipe } from '../pipes/displayPaymentMethodName.pipe';
import { DisplayProgramNamePipe } from '../pipes/displayProgramName.pipe';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Payment } from './payment-model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: 'Home/PaymentInformation',
    providers: [ApplicationService, DataService]

})

export class PaymentInformationComponent implements OnInit {

    /* init private variables */
    private app: Application;
    private availablebillingFrequencies;
    private billingFrequencies;
    private billingFrequencyActive = true;
    private billingStartMonth: string;
    private currentYear = new Date().getFullYear();
    private expirationMonth: string = "";;
    private expirationYear: string = "";
    private firstMonthDue: string;
    private formModel: FormGroup;
    private payment = <Payment>{};
    private today = new Date();
    private totalAmount: number;
    private YEAR_RANGE = 10;
    private years = [{ value: this.currentYear, display: this.currentYear }];

    /* init dropdowns */
    private countries;
    private discounts;
    private discountsTotal = 0;
    private memberDues;
    private states;

    /* constructor */
    constructor(private fb: FormBuilder, public router: Router, private _dataService: DataService, private _applicationService: ApplicationService, private route: ActivatedRoute) {
        this.route.data.subscribe(
            data => { console.log(data) },
            error => console.log(error)
        );
    };

    /* standing data */
    private cardTypes = [
        { value: 'VISA', display: 'Visa' },
        { value: 'MC', display: 'MasterCard' },
        { value: 'AMEX', display: 'American Express' }
    ];

    private invoicingBillingFrequencies = [
        { value: 'annual', display: 'Annual' },
        { value: 'semi-annual', display: 'Semi-Annual' },
    ];

    private months = [
        { value: '01', display: 'January' },
        { value: '02', display: 'February' },
        { value: '03', display: 'March' },
        { value: '04', display: 'April' },
        { value: '05', display: 'May' },
        { value: '06', display: 'June' },
        { value: '07', display: 'July' },
        { value: '08', display: 'August' },
        { value: '09', display: 'September' },
        { value: '10', display: 'October' },
        { value: '11', display: 'November' },
        { value: '12', display: 'December' }
    ];

    private paymentMethods = [
        { value: 'CC', display: 'Recurring Credit Card' },
        { value: 'EFT', display: 'Recurring Electronic Funds Transfer' },
        { value: 'INV', display: 'Invoicing' }
    ];

    private PAYMENT_METHOD_TYPE = {
        CARD: 'card',
        TRANSFER: 'transfer',
        INVOICING: 'invoicing'
    };

    private thankYouPayments = [
        { value: 'DUES', display: 'Dues Offset' },
        { value: 'CASH', display: 'Cash' },
        { value: 'OPT', display: 'Opt Out' }
    ];

    /* OnInit */
    ngOnInit() {
        this.app = this.route.snapshot.data['application'] as Application;
        var objDate = new Date(this.app.startDate),
            locale = "en-us",
            year = objDate.getFullYear(),
            month = objDate.toLocaleString(locale, { month: "long" });
        this.billingStartMonth = month + " " + year;
        this.billingFrequencies = this.app.billingRateOption;

        /* IE will set dropdowns to first value if value is null */
        this.app.methodOfPayment = this.app.methodOfPayment === null ? "" : this.app.methodOfPayment;
        this.app.billingFrequency = this.app.billingFrequency === null ? "" : this.app.billingFrequency;
        this.app.billingInfo.country = this.app.billingInfo.country === null ? "US" : this.app.billingInfo.country;
        this.app.billingInfo.state = this.app.billingInfo.state === null ? "" : this.app.billingInfo.state;
        this.payment.CCType = (this.payment.CCType === null || (!this.payment.CCType)) ? "" : this.payment.CCType;

        if (this.billingFrequencies.length > 0) { this.getFirstMonthDue() };
        this.initYearDropdown();
        this.initPaymentObject();
        this.getDiscounts();
        this.calculateTotal();
        this.getCountries();
    }

    /* OnSubmit */
    onSubmit(paymentForm, payment) {
        this.updateBillingFields();
        if (paymentForm.valid) {
            console.log('form is valid');
            console.log(JSON.stringify(this.app));
            console.log('payment');
            console.log(JSON.stringify(this.payment));
            this.processPayment(payment);
        } else {
            console.log('form is invalid');
            paymentForm.tested = true;
        }
    }


    /* private methods */
    calculateTotal() {
        this.totalAmount = Number(this.app.enrollmentFee) + Number(this.firstMonthDue) - this.discountsTotal;
    }


    filterBillingRateOptions(paymentMethod: string) {
        this.availablebillingFrequencies = this.billingFrequencies.filter(frequency => frequency.methodOfPayment.indexOf(paymentMethod) > -1)
    }

    formatPaymentMethod(paymentMethod: string) {
        if (paymentMethod === 'CC') {
            return "Credit Card";
        } else if (paymentMethod === 'EFT') {
            return "Electronic fund transfer";
        } else if (paymentMethod === 'INV') {
            return "Invoicing";
        }
    }

    /**
      getBillingRateOptions() {
        this._dataService.getBillingRateOption().subscribe(
            data => {
                this.billingFrequencies = data.billingRateOption
                this.availablebillingFrequencies = this.billingFrequencies;
            },
            error => console.log(error),
            () => console.log('donde loading billingRateOptions')
        );
    }
    **/


    getCountries() {
        this._dataService.getCountries().subscribe(
            data => { this.countries = data.countries },
            error => console.log(error),
            () => this.getStates(this.app.billingInfo.country)
        );
    }

    getCountryCode(countryName: string) {
        var country = this.countries.filter(country => country.name === countryName);
        return country[0].countryCode;
    }

    getDiscounts() {
        var appliedDiscounts = this.app.appliedDiscounts as Discount[];
        for (var i = 0; i < appliedDiscounts.length; i++) {
            if (appliedDiscounts[i].type === "Percentage-Monthly") {
                appliedDiscounts[i].amount = (appliedDiscounts[i].percent * Number(this.firstMonthDue)) / 100;
            }
        }
        this.discounts = appliedDiscounts;
        this.getDiscountsTotal();
    }

    getDiscountsTotal() {
        for (var i = 0; i < this.discounts.length; i++) {
            this.discountsTotal += Number(this.discounts[i].amount);
        }
    }

    getFirstMonthDue() {
        var monthlyBillingRateOption = this.billingFrequencies.filter(item => item.frequency === "Monthly");
        this.firstMonthDue = monthlyBillingRateOption[0].amount;
    }

    /*getMemberDues(groupType: string) {
       this._memberDuesService.getMemberDues().subscribe(
           data => { this.memberDues = data[groupType][0] },
           error => console.log(error),
           () => this.getDiscounts()
       );
   }*/

    getStates(countryCode: string) {
        this._dataService.getStates(countryCode).subscribe(
            data => { this.states = data.states },
            error => console.log(error)
        );
    }

    goBack() {
        this.router.navigate(['member-information', { membershipId: this.app.membershipApplicationId }]);
    }

    initPaymentObject() {
        this.payment.FirstName = this.app.person.first;
        this.payment.LastName = this.app.person.last;
        this.payment.Address1 = this.app.company.address1;
        this.payment.Address2 = this.app.company.address2;
        this.payment.City = this.app.company.city;
        this.payment.State = this.app.company.state;
        this.payment.Zip = this.app.company.zip;
        this.payment.CountryCode = this.app.company.country;
        this.payment.Email = this.app.person.email;
        this.payment.MobilePhone = this.app.person.mobilePhone ? this.app.person.mobilePhone : "";
        this.payment.WorkPhone = this.app.person.workPhone;
        this.payment.CustomerCode = this.app.customerCode;
    }

    initYearDropdown() {
        for (var i = 1; i < this.YEAR_RANGE + 1; i++) {
            this.years.push({ value: this.currentYear + i, display: this.currentYear + i });
        }
    };

    processPayment(payment: Payment) {
        this._applicationService.processPayment(payment).subscribe(
            data => { this.countries = data.countries },
            error => console.log(error),
            () => this._applicationService.updateMembership(this.app)
        );
    }

    updateBillingFields() {
        this.payment.PaymentMethod = this.formatPaymentMethod(this.app.methodOfPayment);
        this.payment.CCType = this.payment.CCType;
        this.payment.BillingAddress1 = this.app.billingInfo.address1;
        this.payment.BillingAddress2 = this.app.billingInfo.address2;
        this.payment.BillingCity = this.app.billingInfo.city;
        this.payment.BillingState = this.app.billingInfo.state;
        this.payment.BillingCountryCode = this.app.billingInfo.country;
        this.payment.BillingZip = this.app.billingInfo.zip;
        this.payment.Amount = String((Number(this.app.enrollmentFee) + Number(this.firstMonthDue)) - this.discountsTotal);
        this.payment.AccountName = this.app.billingInfo.billingContactName ? this.app.billingInfo.billingContactName : (this.app.person.first + " " + this.app.person.last);
        this.payment.PhoneExtension = "";
    }

    updateExpirationMonth(month: string) {
        this.payment.CCExpirationData = "";
        this.payment.CCExpirationData = month + this.expirationYear.substring(2, 4);
    }

    updateExpirationYear(year: string) {
        this.payment.CCExpirationData = "";
        this.payment.CCExpirationData = this.expirationMonth + year.substring(2, 4);
    }

    updateState(countryName: string) {
        this.app.company.state = "";
        var countryCode = this.getCountryCode(countryName);
        this.getStates(countryCode);
    }

    get diagnostics() {
        var array: string[] = [JSON.stringify(this.app.billingInfo), JSON.stringify(this.app, ['stepTwoSaved', 'methodOfPayment', 'billingFrequency', 'financialTermsAccepted', 'financialTermsAcceptanceName', 'financialTermsTimestamp']), JSON.stringify(this.payment)];
        return array;
    }

}