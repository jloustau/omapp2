﻿export interface Customer {

    methodOfPayment: string;
    billingFrequency: string;
    billingInfo: {
        billingContactName: string;
        companyName: string;
        address1: string;
        address2: string;
        city: string;
        state: string;
        zip: string;
        country: string;
    };
    enrollmentFee: string;
    financialTermsAccepted: boolean;
    financialTermsAcceptedName: string;
}