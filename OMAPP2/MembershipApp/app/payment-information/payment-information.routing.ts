﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentInformationComponent } from './payment-information.component'
import { PaymentInformationResolve } from '../services/resolvers/payment-information-resolve.service';



const paymentInformationRoutes: Routes = [
    {
        path: '',
        component: PaymentInformationComponent,
        resolve: {
            application: PaymentInformationResolve
        }
    }
];

export const paymentInformationRouting: ModuleWithProviders = RouterModule.forChild(paymentInformationRoutes);