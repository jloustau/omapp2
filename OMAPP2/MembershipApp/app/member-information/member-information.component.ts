﻿import { Application } from '../application.interface';
import { ApplicationService } from '../services/application.service';
import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { FormControl, FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

declare var jQuery: any;

@Component({
    templateUrl: 'Home/MemberInformation',
    providers: [ApplicationService, DataService]
})

export class MemberInformationComponent implements OnInit {

    /* init private variables */
    private app: Application;
    private birthDay: string;
    private birthdayObject: Date;
    private birthdayString: string;
    private birthMonth: string;
    private birthYear: string;
    private currentYear = (new Date().getFullYear()) - 18;
    private industryPlaceholder: string;
    private YEAR_RANGE = 100;
    private formModel: FormGroup;

    /* init dropdowns */
    private application;
    private countries;
    private industries;
    private industryDetails;
    private states;
    private years = [{ value: this.currentYear, display: this.currentYear }];


    /* standing data */
    private annualRevenues = [
        { value: 'Less than 500k', display: 'Less than 500k' },
        { value: '500-999k', display: '500 - 999k' },
        { value: '1-4 Million', display: '1 - 4 Million' },
        { value: '5-9 Million', display: '5 - 9 Million' },
        { value: '10-20 Million', display: '10 - 20 Million' },
        { value: '21-49 Million', display: '21 - 49 Million' },
        { value: '50-99 Million', display: '50 - 99 Million' },
        { value: '100-249 Million', display: '100 - 249 Million' },
        { value: '250-499 Million', display: '250 - 499 Million' },
        { value: '500-999 Million', display: '500 - 999 Million' },
        { value: '1+ Billion', display: '1+ Billion' }
    ];

    private annualRevenuesValues = ['Less than 500k', '500-999k', '1-4 Million', '5-9 Million',
        '10-20 Million', '21-49 Million', '50-99 Million', '100-249 Million', '250-499 Million',
        '500-999 Million', '1+ Billion'];

    private commonDays = [{ value: '01', display: 1 }];

    private genders = [
        { value: 'M', display: 'Male' },
        { value: 'F', display: 'Female' },
    ];

    private months = [
        { value: '01', display: 'January' },
        { value: '02', display: 'February' },
        { value: '03', display: 'March' },
        { value: '04', display: 'April' },
        { value: '05', display: 'May' },
        { value: '06', display: 'June' },
        { value: '07', display: 'July' },
        { value: '08', display: 'August' },
        { value: '09', display: 'September' },
        { value: '10', display: 'October' },
        { value: '11', display: 'November' },
        { value: '12', display: 'December' }
    ];

    private numbersOfEmployees = [
        { value: '0', display: '0' },
        { value: '1-9', display: '1 - 9' },
        { value: '10-19', display: '10 - 19' },
        { value: '20-49', display: '20 - 49' },
        { value: '50-99', display: '50 - 99' },
        { value: '100-499', display: '100 - 499' },
        { value: '500-999', display: '500 - 999' },
        { value: '1000-4999', display: '1000 - 4999' },
        { value: '5000-9999', display: '5000 - 9999' },
        { value: '10000+', display: '10000+' }
    ];

    private numbersOfEmployeesValues = ['0', '1-9', '10-19', '20-49', '500-999', '1000-4999', '5000-9999', '10000+'];

    private prefixes = [
        { value: 'Mr', display: 'Mr.' },
        { value: 'Mrs', display: 'Mrs.' },
        { value: 'Ms', display: 'Ms.' },
        { value: 'Dr', display: 'Dr.' },
        { value: 'Prof', display: 'Prof.' }
    ];

    private referralTypes = [
        { value: 'Member', display: 'Referred by Vistage Member' },
        { value: 'Chair', display: 'Referred by Vistage Chair' },
        { value: 'Speaker', display: 'Referred by Vistage Speaker' },
        { value: 'Staff', display: 'Referred by Vistage Staff' },
        { value: 'Advisor', display: 'Referred by Business Advisor' },
        { value: 'TradeAssociation', display: 'Referred by Trade Association' },
        { value: 'Mailing', display: 'Received a Mailing' },
        { value: 'Event', display: 'Attended a Vistage Event' },
        { value: 'Ad', display: 'Responded to an Ad' }

    ];

    private typesOfOrgs = [
        { value: 'Public', display: 'Public' },
        { value: 'Private', display: 'Private' },
        { value: 'Not-for-profit', display: 'Not-for-profit' },
        { value: 'Government', display: 'Government' }
    ];

    private typesOfOrgsValues = ['Public', 'Private', 'Not-for-profit', 'Government'];

    /* constructor */
    constructor(private fb: FormBuilder, public router: Router, private _dataService: DataService, private _applicationService: ApplicationService, private route: ActivatedRoute) {
        this.route.data.subscribe(
            data => { console.log(data) },
            error => console.log(error)
        );
    };


    /* OnInit */
    ngOnInit() {
        this.app = this.route.snapshot.data['application'] as Application;
        this.birthdayString = this.app.person.birthday;

        /* IE will set dropdowns to first value if value is null */
        this.app.person.prefix = this.app.person.prefix === null ? "" : this.app.person.prefix;
        this.app.person.gender = this.app.person.gender === null ? "" : this.app.company.country;
        this.industryPlaceholder = (this.industryPlaceholder === null || (!this.industryPlaceholder)) ? "" : this.industryPlaceholder;
        this.app.company.industryNAICS = (this.app.company.industryNAICS === null || Number.isNaN(Number(this.app.company.industryNAICS))) ? "" : this.app.company.industryNAICS;
        this.app.company.country = this.app.company.country === null ? "US" : this.app.company.country;
        this.app.company.state = this.app.company.state === null ? "" : this.app.company.country;
        this.app.company.typeOfOrg = this.app.company.typeOfOrg === null ? "" : this.app.company.typeOfOrg;
        this.app.company.typeOfOrg = (this.typesOfOrgsValues.indexOf(this.app.company.typeOfOrg) > -1) ? this.app.company.typeOfOrg : "";
        this.app.company.annualRevenue = this.app.company.annualRevenue === null ? "" : this.app.company.annualRevenue;
        this.app.company.annualRevenue = (this.annualRevenuesValues.indexOf(this.app.company.annualRevenue) > -1) ? this.app.company.annualRevenue : "";
        this.app.company.numberOfEmployee = this.app.company.numberOfEmployee === null ? "" : this.app.company.numberOfEmployee;
        this.app.company.numberOfEmployee = (this.numbersOfEmployeesValues.indexOf(this.app.company.numberOfEmployee) > -1) ? this.app.company.numberOfEmployee : "";
        this.app.person.referralType = this.app.person.referralType === null ? "" : this.app.person.referralType;

        console.log("this app");
        console.log(this.app);

        this.getIndustries();
        this.initBirthday();
        this.initDayDropdown();
        this.initYearDropdown();
        this.getCountries();
    }

    /* OnSubmit */
    onSubmit(memberForm) {
        if (memberForm.valid) {
            console.log('form is valid');
            this.app.stepTwoSaved = true;
            this.updateMembership(this.app);
            console.log(this.app.membershipApplicationId);
            this.router.navigate(['payment-information', { membershipId: this.app.membershipApplicationId }]);

        } else {
            console.log('form is invalid');
            memberForm.tested = true;
        }
    }

    /* private methods */

    changeOnForm(memberForm) {
        if (!memberForm.valid) {
            this.app.stepTwoSaved = false;
        }
    }

    getApplication() {
        this._dataService.getMembershipApp().subscribe(
            data => { this.application = data },
            error => console.log(error)
        );
    }

    getCountries() {
        this._dataService.getCountries().subscribe(
            data => { this.countries = data.countries },
            error => console.log(error),
            /*() => this.getStates(this.getCountryCode(this.app.company.country))*/
            () => this.getStates(this.app.company.country)
        );
    }

    getCountryCode(countryName: string) {
        var country = this.countries.filter(country => country.name === countryName);
        return country[0].countryCode;
    }

    getIndustries() {
        this._dataService.getIndustries().subscribe(
            data => { this.industries = data.industries },
            error => console.log(error),
            () => this.initIndustriesDropdowns()
        );
    }

    getIndustryDetails(industryCode: number) {
        industryCode = Number(industryCode);
        this.industryDetails = this.industries.filter(industry => industry.code === industryCode);
        this.industryDetails = this.industryDetails[0].industryDetail;
    }

    getIndustryParent(industryDetail: string) {
        var parent: string;
        for (var i = 0; i < this.industries.length; i++) {
            for (var j = 0; j < this.industries[i].industryDetail.length; j++) {
                if (this.industries[i].industryDetail[j].code === Number(industryDetail)) {
                    parent = this.industries[i].code;
                }
            }
        }
        return parent;
    }

    getStates(countryCode: string) {
        this._dataService.getStates(countryCode).subscribe(
            data => { this.states = data.states },
            error => console.log(error)
        );
    }

    goBack() {
        this.router.navigate(['membership-terms', { membershipId: this.app.membershipApplicationId }]);
    }

    initBirthday() {
        if (this.birthdayString) {
            this.birthdayObject = new Date(this.app.person.birthday);
            this.birthYear = String(this.birthdayObject.getFullYear());
            this.birthMonth = ('0' + (this.birthdayObject.getMonth() + 1)).slice(-2);
            this.birthDay = ('0' + this.birthdayObject.getDate()).slice(-2);
        } else {
            this.birthYear = "";
            this.birthMonth = "";
            this.birthDay = "";
        }
    }

    initDayDropdown() {
        for (var i = 2; i < 29; i++) {
            this.commonDays.push({ value: ('0' + i).slice(-2), display: i });
        }
    }

    initIndustriesDropdowns() {
        if (this.app.company.industryNAICS !== "" && this.app.company.industryNAICS !== null) {
            this.industryPlaceholder = this.getIndustryParent(this.app.company.industryNAICS);
            this.getIndustryDetails(Number(this.industryPlaceholder));
        }
    };

    initYearDropdown() {
        for (var i = 1; i <= this.YEAR_RANGE; i++) {
            this.years.push({ value: this.currentYear - i, display: this.currentYear - i });
        }
    }

    isLeapYear(year: string) {
        var numYear = Number(year);
        var isLeapYear = ((numYear % 4 == 0) && (numYear % 100 != 0)) || (numYear % 400 == 0);
        return String(isLeapYear);
    }

    updatebirthMonth(month: string) {
        this.app.person.birthday = month + "/" + this.birthDay + "/" + this.birthYear;
    }

    updatebirthDay(day: string) {
        this.app.person.birthday = this.birthMonth + "/" + day + "/" + this.birthYear;
    }

    updatebirthYear(year: string) {
        this.app.person.birthday = this.birthMonth + "/" + this.birthDay + "/" + year;
    }

    updateMembership(app: Application) {
        this._applicationService.updateMembership(app).subscribe(
            error => console.log(error),
            () => console.log('updateSuccessful')
        );
    }

    updateState(countryName: string) {
        this.app.company.state = "";
        /*var countryCode = this.getCountryCode(countryName);*/
        /*this.getStates(countryCode);*/
        this.getStates(countryName);
    }

    get diagnostics() {
        var array: string[] = [JSON.stringify(this.app.person), JSON.stringify(this.app.company), JSON.stringify(this.app, ['stepOneSaved', 'termsAcceptedTimestamp', 'stepTwoSaved'])];
        return array;
    }
}

