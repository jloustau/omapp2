"use strict";
var router_1 = require('@angular/router');
var member_information_component_1 = require('./member-information.component');
var member_information_resolve_service_1 = require('../services/resolvers/member-information-resolve.service');
var memberInformationRoutes = [
    {
        path: '',
        component: member_information_component_1.MemberInformationComponent,
        resolve: {
            application: member_information_resolve_service_1.MemberInformationResolve
        }
    }
];
exports.memberInformationRouting = router_1.RouterModule.forChild(memberInformationRoutes);
//# sourceMappingURL=member-information.routing.js.map