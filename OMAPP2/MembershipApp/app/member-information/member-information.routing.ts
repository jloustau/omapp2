﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MemberInformationComponent } from './member-information.component'
import { MemberInformationResolve } from '../services/resolvers/member-information-resolve.service';

const memberInformationRoutes: Routes = [
    {
        path: '',
        component: MemberInformationComponent,
        resolve: {
            application: MemberInformationResolve
        }
    }
];

export const memberInformationRouting: ModuleWithProviders = RouterModule.forChild(memberInformationRoutes);