﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MemberInformationComponent } from './member-information.component'
import { memberInformationRouting } from './member-information.routing';
import { MemberInformationResolve } from '../services/resolvers/member-information-resolve.service';

@NgModule({

    declarations: [MemberInformationComponent],
    imports: [CommonModule, FormsModule, ReactiveFormsModule, memberInformationRouting],
    exports: [MemberInformationComponent],
    providers: [MemberInformationResolve]
})

export class MemberInformationModule {
}