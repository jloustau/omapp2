"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var application_service_1 = require('../services/application.service');
var core_1 = require('@angular/core');
var data_service_1 = require('../services/data.service');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var MemberInformationComponent = (function () {
    /* constructor */
    function MemberInformationComponent(fb, router, _dataService, _applicationService, route) {
        this.fb = fb;
        this.router = router;
        this._dataService = _dataService;
        this._applicationService = _applicationService;
        this.route = route;
        this.currentYear = (new Date().getFullYear()) - 18;
        this.YEAR_RANGE = 100;
        this.years = [{ value: this.currentYear, display: this.currentYear }];
        /* standing data */
        this.annualRevenues = [
            { value: 'Less than 500k', display: 'Less than 500k' },
            { value: '500-999k', display: '500 - 999k' },
            { value: '1-4 Million', display: '1 - 4 Million' },
            { value: '5-9 Million', display: '5 - 9 Million' },
            { value: '10-20 Million', display: '10 - 20 Million' },
            { value: '21-49 Million', display: '21 - 49 Million' },
            { value: '50-99 Million', display: '50 - 99 Million' },
            { value: '100-249 Million', display: '100 - 249 Million' },
            { value: '250-499 Million', display: '250 - 499 Million' },
            { value: '500-999 Million', display: '500 - 999 Million' },
            { value: '1+ Billion', display: '1+ Billion' }
        ];
        this.annualRevenuesValues = ['Less than 500k', '500-999k', '1-4 Million', '5-9 Million',
            '10-20 Million', '21-49 Million', '50-99 Million', '100-249 Million', '250-499 Million',
            '500-999 Million', '1+ Billion'];
        this.commonDays = [{ value: '01', display: 1 }];
        this.genders = [
            { value: 'M', display: 'Male' },
            { value: 'F', display: 'Female' },
        ];
        this.months = [
            { value: '01', display: 'January' },
            { value: '02', display: 'February' },
            { value: '03', display: 'March' },
            { value: '04', display: 'April' },
            { value: '05', display: 'May' },
            { value: '06', display: 'June' },
            { value: '07', display: 'July' },
            { value: '08', display: 'August' },
            { value: '09', display: 'September' },
            { value: '10', display: 'October' },
            { value: '11', display: 'November' },
            { value: '12', display: 'December' }
        ];
        this.numbersOfEmployees = [
            { value: '0', display: '0' },
            { value: '1-9', display: '1 - 9' },
            { value: '10-19', display: '10 - 19' },
            { value: '20-49', display: '20 - 49' },
            { value: '50-99', display: '50 - 99' },
            { value: '100-499', display: '100 - 499' },
            { value: '500-999', display: '500 - 999' },
            { value: '1000-4999', display: '1000 - 4999' },
            { value: '5000-9999', display: '5000 - 9999' },
            { value: '10000+', display: '10000+' }
        ];
        this.numbersOfEmployeesValues = ['0', '1-9', '10-19', '20-49', '500-999', '1000-4999', '5000-9999', '10000+'];
        this.prefixes = [
            { value: 'Mr', display: 'Mr.' },
            { value: 'Mrs', display: 'Mrs.' },
            { value: 'Ms', display: 'Ms.' },
            { value: 'Dr', display: 'Dr.' },
            { value: 'Prof', display: 'Prof.' }
        ];
        this.referralTypes = [
            { value: 'Member', display: 'Referred by Vistage Member' },
            { value: 'Chair', display: 'Referred by Vistage Chair' },
            { value: 'Speaker', display: 'Referred by Vistage Speaker' },
            { value: 'Staff', display: 'Referred by Vistage Staff' },
            { value: 'Advisor', display: 'Referred by Business Advisor' },
            { value: 'TradeAssociation', display: 'Referred by Trade Association' },
            { value: 'Mailing', display: 'Received a Mailing' },
            { value: 'Event', display: 'Attended a Vistage Event' },
            { value: 'Ad', display: 'Responded to an Ad' }
        ];
        this.typesOfOrgs = [
            { value: 'Public', display: 'Public' },
            { value: 'Private', display: 'Private' },
            { value: 'Not-for-profit', display: 'Not-for-profit' },
            { value: 'Government', display: 'Government' }
        ];
        this.typesOfOrgsValues = ['Public', 'Private', 'Not-for-profit', 'Government'];
        this.route.data.subscribe(function (data) { console.log(data); }, function (error) { return console.log(error); });
    }
    ;
    /* OnInit */
    MemberInformationComponent.prototype.ngOnInit = function () {
        this.app = this.route.snapshot.data['application'];
        this.birthdayString = this.app.person.birthday;
        /* IE will set dropdowns to first value if value is null */
        this.app.person.prefix = this.app.person.prefix === null ? "" : this.app.person.prefix;
        this.app.person.gender = this.app.person.gender === null ? "" : this.app.company.country;
        this.industryPlaceholder = (this.industryPlaceholder === null || (!this.industryPlaceholder)) ? "" : this.industryPlaceholder;
        this.app.company.industryNAICS = (this.app.company.industryNAICS === null || Number.isNaN(Number(this.app.company.industryNAICS))) ? "" : this.app.company.industryNAICS;
        this.app.company.country = this.app.company.country === null ? "US" : this.app.company.country;
        this.app.company.state = this.app.company.state === null ? "" : this.app.company.country;
        this.app.company.typeOfOrg = this.app.company.typeOfOrg === null ? "" : this.app.company.typeOfOrg;
        this.app.company.typeOfOrg = (this.typesOfOrgsValues.indexOf(this.app.company.typeOfOrg) > -1) ? this.app.company.typeOfOrg : "";
        this.app.company.annualRevenue = this.app.company.annualRevenue === null ? "" : this.app.company.annualRevenue;
        this.app.company.annualRevenue = (this.annualRevenuesValues.indexOf(this.app.company.annualRevenue) > -1) ? this.app.company.annualRevenue : "";
        this.app.company.numberOfEmployee = this.app.company.numberOfEmployee === null ? "" : this.app.company.numberOfEmployee;
        this.app.company.numberOfEmployee = (this.numbersOfEmployeesValues.indexOf(this.app.company.numberOfEmployee) > -1) ? this.app.company.numberOfEmployee : "";
        this.app.person.referralType = this.app.person.referralType === null ? "" : this.app.person.referralType;
        console.log("this app");
        console.log(this.app);
        this.getIndustries();
        this.initBirthday();
        this.initDayDropdown();
        this.initYearDropdown();
        this.getCountries();
    };
    /* OnSubmit */
    MemberInformationComponent.prototype.onSubmit = function (memberForm) {
        if (memberForm.valid) {
            console.log('form is valid');
            this.app.stepTwoSaved = true;
            this.updateMembership(this.app);
            console.log(this.app.membershipApplicationId);
            this.router.navigate(['payment-information', { membershipId: this.app.membershipApplicationId }]);
        }
        else {
            console.log('form is invalid');
            memberForm.tested = true;
        }
    };
    /* private methods */
    MemberInformationComponent.prototype.changeOnForm = function (memberForm) {
        if (!memberForm.valid) {
            this.app.stepTwoSaved = false;
        }
    };
    MemberInformationComponent.prototype.getApplication = function () {
        var _this = this;
        this._dataService.getMembershipApp().subscribe(function (data) { _this.application = data; }, function (error) { return console.log(error); });
    };
    MemberInformationComponent.prototype.getCountries = function () {
        var _this = this;
        this._dataService.getCountries().subscribe(function (data) { _this.countries = data.countries; }, function (error) { return console.log(error); }, 
        /*() => this.getStates(this.getCountryCode(this.app.company.country))*/
        function () { return _this.getStates(_this.app.company.country); });
    };
    MemberInformationComponent.prototype.getCountryCode = function (countryName) {
        var country = this.countries.filter(function (country) { return country.name === countryName; });
        return country[0].countryCode;
    };
    MemberInformationComponent.prototype.getIndustries = function () {
        var _this = this;
        this._dataService.getIndustries().subscribe(function (data) { _this.industries = data.industries; }, function (error) { return console.log(error); }, function () { return _this.initIndustriesDropdowns(); });
    };
    MemberInformationComponent.prototype.getIndustryDetails = function (industryCode) {
        industryCode = Number(industryCode);
        this.industryDetails = this.industries.filter(function (industry) { return industry.code === industryCode; });
        this.industryDetails = this.industryDetails[0].industryDetail;
    };
    MemberInformationComponent.prototype.getIndustryParent = function (industryDetail) {
        var parent;
        for (var i = 0; i < this.industries.length; i++) {
            for (var j = 0; j < this.industries[i].industryDetail.length; j++) {
                if (this.industries[i].industryDetail[j].code === Number(industryDetail)) {
                    parent = this.industries[i].code;
                }
            }
        }
        return parent;
    };
    MemberInformationComponent.prototype.getStates = function (countryCode) {
        var _this = this;
        this._dataService.getStates(countryCode).subscribe(function (data) { _this.states = data.states; }, function (error) { return console.log(error); });
    };
    MemberInformationComponent.prototype.goBack = function () {
        this.router.navigate(['membership-terms', { membershipId: this.app.membershipApplicationId }]);
    };
    MemberInformationComponent.prototype.initBirthday = function () {
        if (this.birthdayString) {
            this.birthdayObject = new Date(this.app.person.birthday);
            this.birthYear = String(this.birthdayObject.getFullYear());
            this.birthMonth = ('0' + (this.birthdayObject.getMonth() + 1)).slice(-2);
            this.birthDay = ('0' + this.birthdayObject.getDate()).slice(-2);
        }
        else {
            this.birthYear = "";
            this.birthMonth = "";
            this.birthDay = "";
        }
    };
    MemberInformationComponent.prototype.initDayDropdown = function () {
        for (var i = 2; i < 29; i++) {
            this.commonDays.push({ value: ('0' + i).slice(-2), display: i });
        }
    };
    MemberInformationComponent.prototype.initIndustriesDropdowns = function () {
        if (this.app.company.industryNAICS !== "" && this.app.company.industryNAICS !== null) {
            this.industryPlaceholder = this.getIndustryParent(this.app.company.industryNAICS);
            this.getIndustryDetails(Number(this.industryPlaceholder));
        }
    };
    ;
    MemberInformationComponent.prototype.initYearDropdown = function () {
        for (var i = 1; i <= this.YEAR_RANGE; i++) {
            this.years.push({ value: this.currentYear - i, display: this.currentYear - i });
        }
    };
    MemberInformationComponent.prototype.isLeapYear = function (year) {
        var numYear = Number(year);
        var isLeapYear = ((numYear % 4 == 0) && (numYear % 100 != 0)) || (numYear % 400 == 0);
        return String(isLeapYear);
    };
    MemberInformationComponent.prototype.updatebirthMonth = function (month) {
        this.app.person.birthday = month + "/" + this.birthDay + "/" + this.birthYear;
    };
    MemberInformationComponent.prototype.updatebirthDay = function (day) {
        this.app.person.birthday = this.birthMonth + "/" + day + "/" + this.birthYear;
    };
    MemberInformationComponent.prototype.updatebirthYear = function (year) {
        this.app.person.birthday = this.birthMonth + "/" + this.birthDay + "/" + year;
    };
    MemberInformationComponent.prototype.updateMembership = function (app) {
        this._applicationService.updateMembership(app).subscribe(function (error) { return console.log(error); }, function () { return console.log('updateSuccessful'); });
    };
    MemberInformationComponent.prototype.updateState = function (countryName) {
        this.app.company.state = "";
        /*var countryCode = this.getCountryCode(countryName);*/
        /*this.getStates(countryCode);*/
        this.getStates(countryName);
    };
    Object.defineProperty(MemberInformationComponent.prototype, "diagnostics", {
        get: function () {
            var array = [JSON.stringify(this.app.person), JSON.stringify(this.app.company), JSON.stringify(this.app, ['stepOneSaved', 'termsAcceptedTimestamp', 'stepTwoSaved'])];
            return array;
        },
        enumerable: true,
        configurable: true
    });
    MemberInformationComponent = __decorate([
        core_1.Component({
            templateUrl: 'Home/MemberInformation',
            providers: [application_service_1.ApplicationService, data_service_1.DataService]
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, router_1.Router, data_service_1.DataService, application_service_1.ApplicationService, router_1.ActivatedRoute])
    ], MemberInformationComponent);
    return MemberInformationComponent;
}());
exports.MemberInformationComponent = MemberInformationComponent;
//# sourceMappingURL=member-information.component.js.map