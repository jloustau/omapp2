"use strict";
var router_1 = require('@angular/router');
var appRoutes = [
    {
        path: 'membership-terms',
        loadChildren: 'app/membership-commitment/membership-commitment.module#MembershipCommitmentModule'
    },
    {
        path: 'member-information',
        loadChildren: 'app/member-information/member-information.module#MemberInformationModule'
    },
    {
        path: 'payment-information',
        loadChildren: 'app/payment-information/payment-information.module#PaymentInformationModule'
    },
    {
        path: ':token',
        loadChildren: 'app/home/home.module#HomeModule'
    },
    {
        path: '',
        redirectTo: 'membership-terms',
        pathMatch: 'full'
    }
];
exports.appRouting = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map