﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HomeComponent } from './home.component'
import { homeRouting } from './home.routing';
import { InitialResolve } from '../services/resolvers/initial-resolve.service';



@NgModule({

    declarations: [HomeComponent],
    imports: [CommonModule, FormsModule, ReactiveFormsModule, homeRouting],
    exports: [HomeComponent],
    providers: [InitialResolve]
})

export class HomeModule {
}