﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component'
import { InitialResolve } from '../services/resolvers/initial-resolve.service';



const homeRoutes: Routes = [
    {
        path: '',
        component: HomeComponent,
        resolve: {
            application: InitialResolve
        }
    }
];

export const homeRouting: ModuleWithProviders = RouterModule.forChild(homeRoutes);