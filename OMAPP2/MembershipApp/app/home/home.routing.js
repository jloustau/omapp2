"use strict";
var router_1 = require('@angular/router');
var home_component_1 = require('./home.component');
var initial_resolve_service_1 = require('../services/resolvers/initial-resolve.service');
var homeRoutes = [
    {
        path: '',
        component: home_component_1.HomeComponent,
        resolve: {
            application: initial_resolve_service_1.InitialResolve
        }
    }
];
exports.homeRouting = router_1.RouterModule.forChild(homeRoutes);
//# sourceMappingURL=home.routing.js.map