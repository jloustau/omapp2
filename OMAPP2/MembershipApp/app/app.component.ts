import { Component, Input } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormGroup, FormBuilder, Validators }  from '@angular/forms';
/*import { APP } from './application';*/
import { APP } from './emptyApplication';

@Component({
    selector: 'my-app',
    template: `
       <router-outlet></router-outlet>
    `,
})
export class AppComponent {
    app = APP;
}
