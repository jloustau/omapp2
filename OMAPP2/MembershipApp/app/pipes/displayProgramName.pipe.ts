﻿import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'displayProgramName' })
export class DisplayProgramNamePipe implements PipeTransform {
    transform(value: string): any {
        if (!value) return value;
        if (value === 'CE') return 'chief executive';
        if (value === 'TA') return 'trusted advisor';
        if (value === 'VI') return 'vistage inside';
    }
}