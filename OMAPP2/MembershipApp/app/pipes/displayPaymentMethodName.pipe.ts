﻿import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'displayPaymentMethodName' })
export class DisplayPaymentMethodNamePipe implements PipeTransform {
    transform(value: string): any {
        if (!value) return value;
        if (value === 'CC') return 'credit card';
        if (value === 'INV') return 'invoicing';
        if (value === 'EFT') return 'electronic funds transfer';
    }
}