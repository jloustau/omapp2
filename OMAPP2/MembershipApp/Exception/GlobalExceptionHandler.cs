﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace MembershipApp.Exception
{
    public class GlobalExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            // This is how we would filter only certain exceptions and pass a message to the caller
            //if (context.Exception is ArgumentNullException)
            //{
            //    var result = new HttpResponseMessage(HttpStatusCode.BadRequest)
            //    {
            //        Content = new StringContent(context.Exception.Message),
            //        ReasonPhrase = "ArgumentNullException"
            //    };

            //    context.Result = new ArgumentNullResult(context.Request, result);
            //}
            //else
            //{
            //    // Handle other exceptions, do other things
            //}
        }

        public class ArgumentNullResult : IHttpActionResult
        {
            private HttpRequestMessage _request;
            private readonly HttpResponseMessage _httpResponseMessage;

            public ArgumentNullResult(HttpRequestMessage request, HttpResponseMessage httpResponseMessage)
            {
                _request = request;
                _httpResponseMessage = httpResponseMessage;
            }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                return Task.FromResult(_httpResponseMessage);
            }
        }
    }
}