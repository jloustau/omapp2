﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MembershipApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                name: "Membership",
                url: "Home/MembershipCommitment/{id}",
                defaults: new { controller = "Home", action = "MembershipCommitment", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Member",
               url: "Home/MemberInformation/{id}",
               defaults: new { controller = "Home", action = "MemberInformation", id = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "Payment",
               url: "Home/PaymentInformation/{id}",
               defaults: new { controller = "Home", action = "PaymentInformation", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                name: "Spa-fallback",
                url: "{*url}",
                defaults: new { controller = "Home", action = "Index" }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );


        }
    }
}
