﻿using System.Web.Http.ExceptionHandling;

namespace MembershipApp.Logging
{
    public class UnhandledExceptionLogger : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            var log = context.Exception.ToString();
            //Do whatever logging you need to do here.
            Globals.Logger.Error("Received an unhanlded exception.", context.Exception);
        }
    }
}